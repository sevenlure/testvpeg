import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import { infoSetting } from 'models/Base'
import Facility from './Facility'

const Schema = mongoose.Schema

var FacilityEnviProtectFee = new mongoose.Schema({
  ...infoSetting,
  FacilityID: Number,
  facility: {
    type: Schema.Types.ObjectId,
    ref: 'Facility',
    autopopulate: true
  },
  IncurredAddress: String,
  Phone: Number,
  Email: String,
  Fax: String,
  YearOfCharge: Number,
  QuaterOfCharge: String,
  SubjectOfCharge: String,
  AuthorityBVMT: String,
  Indebtedness: Number,
  PaymentStatus: String,
  Payment: Number,

  AverageWaterDemand: Number,  //remove
  AverageWasteWater: Number,  //remove
  AverageWasteWaterEnviProfile: Number,  //remove
  AverageWasteWaterActual :Number,  //remove
  TotalWasteWaterOfQuarter:  Number,  //remove
  WastewaterContent: Array,  //remove


  // NOTE  cap nhat
  TenCanBoThuLy: String, // Tên cán bộ thụ lý hồ sơ
  QuyMo: String,
  LuuLuongNuocThai: String,
  Quy: String,
  Nam: Number,
  NgayNopToKhai: Date,
  NgayCoVanBanThongBao: Date,
  SoHieuVanBanTraLoi: String,
  ThongBaoLan: String,
  LoaiThongBao: String,
  TongTienTheoThongBaoNopPhi: Number,
  TinhTrangNop: String,
  NamThongBaoNopPhi: Number,
})
FacilityEnviProtectFee.plugin(autopopulate)

var FacilityEnviProtectFeeSimple = new mongoose.Schema({
  ...infoSetting,
  facility: {
    type: Schema.Types.ObjectId,
    ref: 'Facility',
    autopopulate: true
  },
  IncurredAddress: String, //Địa chỉ phát sinh nước thải
  Phone: Number,
  Email: String,
  Fax: String,
  YearOfCharge: Number, 
  QuaterOfCharge: String, //Quý tính phí
  SubjectOfCharge: String, //Thuộc đối tượng thu phí
  AverageWaterDemand: Number, //Nhu cầu sử dụng nước 
  AuthorityBVMT: String,
  AverageWasteWater: Number, //Lưu lượng nước thải phát sinh trung bình
  AverageWasteWaterEnviProfile: Number, //Lượng nước thải phát sinh theo hồ sơ môi trường
  AverageWasteWaterActual :Number, //Lượng nước thải phát sinh theo thực tế
  TotalWasteWaterOfQuarter:  Number, //Tổng lượng nước thải phát sinh trong quý
  WastewaterContent: Array,
  Payment: Number,

  // NOTE  cap nhat
  TenCanBoThuLy: String, // Tên cán bộ thụ lý hồ sơ
  QuyMo: String,
  LuuLuongNuocThai: String,
  Quy: String,
  Nam: Number,
  NgayNopToKhai: Date,
  NgayCoVanBanThongBao: Date,
  SoHieuVanBanTraLoi: String,
  ThongBaoLan: String,
  LoaiThongBao: String,
  TongTienTheoThongBaoNopPhi: Number,
  TinhTrangNop: String,
  NamThongBaoNopPhi: Number,
})

export const FacilityEnviProtectFeeSimpleModel =  mongoose.model('FacilityEnviProtectFeeSimpleModel', FacilityEnviProtectFeeSimple, 'facilityenviprotectfees',)

export default mongoose.model('FacilityEnviProtectFee', FacilityEnviProtectFee)
