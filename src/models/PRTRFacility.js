import mongoose from 'mongoose'
import { infoSetting } from './Base'
import autopopulate from 'mongoose-autopopulate'

const Schema = mongoose.Schema

var PRTRFacilitySchema = new mongoose.Schema({
  ...infoSetting,
  facility: {
    type: Schema.Types.ObjectId,
    ref: 'Facility',
    autopopulate: true
  },
  YYYY: { type: Schema.Types.Number, default: null } , 
  MM:{ type: Schema.Types.Number, default: null } ,
  ngayHoatDong: { type: Schema.Types.Number, default: 365 },
  donViPhanTich: String,
  ngayLayMau: Date,
  ngayTraKetQua: Date,
  listLoDot: [
    {
      tenLoDot: { type: Schema.Types.String, default: null },
      luuLuong: { type: Schema.Types.Number, default: null },
      hoachats: [
        {
          value: { type: Schema.Types.Number, default: null },
          hoaChat: 
            {
              type: Schema.Types.ObjectId,
              default: null,
              ref: 'Lookuplistitem',
              autopopulate: true
            }
          
        }
      ]
    }
  ]
})
PRTRFacilitySchema.plugin(autopopulate)

export default mongoose.model('PRTRFacility', PRTRFacilitySchema)
