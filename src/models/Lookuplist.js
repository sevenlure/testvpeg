import mongoose from 'mongoose'
import { infoSetting } from './Base'

const Lookuplist = new mongoose.Schema({
  ...infoSetting,
  LookupListID: Number,
  Name: String,
  NameEnglish: String,
  IsInUse: { type: Boolean, default: true },
  IsHasExtra: { type: Boolean, default: false },
  IsHasChildren: { type: Boolean, default: false },
  referenced: {
    collection: String,
    path: String
  }
})

export default mongoose.model('Lookuplist', Lookuplist)
