import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import Facilityeia from './Facilityeia'
import Facilitypermit from './Facilitypermit'
import Facilityemission from './Facilityemission'
import FacilityEnviProtectFee from './FacilityEnviProtectFee'
import FacilityInspection from './FacilityInspection'
import FacilityHazarWasteReport from './FacilityHazarWasteReport'
import { infoSetting } from './Base'

const Schema = mongoose.Schema;

var Facility = new mongoose.Schema({
  ...infoSetting,
  Taxcode: Number,
  Branchcode: String,
  FacilityCode: { type: String, unique: true },
  FacilityID: Number,
  FacilityNameFull: String,
  FacilityNameBrief: String,
  IndustrialPark: String,
  IndustrialAreaID: Number,
  industrialArea: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  Latitude: Number,
  Longitude: Number,
  ProvinceID: Number,
  province: { type: Schema.Types.ObjectId, ref: 'Province', autopopulate: true },
  SectorID: Number,
  sector: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  Telephone: Number,
  Fax: Number,
  Aream2: Number, // Diện tích
  Address1: String,
  Address2: String,
  Commune: String, // Phuong xa
  Remarks: String,
  InvestmentCapital: Number,
  Employees: Number, // So luong nhan vien
  Capacity: String, // Công Suất
  ProductDesc: String, // Quy trình sản xuất
  Fuel: String,  //Nhiên liệu
  MainMaterials: String, // Vật liệu chính
  MaterialConsumption: String, //Vat lieu tieu thu
  WorkingDaysMonth: Number,  //Số ngày làm việc trong tháng
  WorkingDaysYear: Number, //Số ngày làm việc trong năm
  WaterUseDay: Number,  // Lượng nước sử dụng trong ngày
  WaterUseMonth: Number,  // Lượng nước sử dụng trong tháng
  DistrictID: Number,
  PContactFirst: String, //Ten Can Bo phu trach moi truongtruong
  PContactTel: Number,  //
  PContactEmail: String,  //
  SContactFirst: String, //Nguoi chiu trach nhiem phap luat
  SContactTel: Number,
  SContactEmail: String,
  district: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  InvestmentLicenseIssueDateFirst: Date, //Ngay cap phep Giay Chung Nhan dau tu
  InvestmentLicenseIssueDateLast: Date, //Ngay cap phep Giay Chung Nhan dau tu
  InvestmentLicenseNumber: Number, // So cap phep Giay Chung Nhan Dau tu
  BusinessLicenseIssueDateFirst: Date,  // Ngay cap phep Giay dang ky kiunh doanh
  BusinessLicenseIssueDateLast: Date,  // Ngay cap phep Giay dang ky kiunh doanh
  BusinessLicenseNumber: String, // So cap phep Giay dang ky kiunh doanh// Có thay đổi yeu cau 5/3/2019
  YearsOfOperation: Number,  //Nam  hoat dong
  AuthorityID: Number,
  authority: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true }, // Tham Quyen Quan ly moi truong
  WaterSourceID: Number,
  waterSource: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true }, // Nguon Nuoc
  OwnershipID: Number,
  ownership: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true }, // Hinh Thu So Huu
  //Các truong them moi
  ReceviedSource_1: Object, // Nguon tiep nhan 1
  ReceviedSource_2: Object,// Nguon tiep nhan 2
  ReceviedSource_3: Object,// Nguon tiep nhan 3
  ReceviedSource_4: Object,// Nguon tiep nhan 4

  FrequencyOfMonitoring: String, // Tần suất giám sát

  //refs
  facilityeias : [{ type: Schema.Types.ObjectId, ref: 'Facilityeia', autopopulate: { maxDepth: 2 } }],
  facilityinspections : [{ type: Schema.Types.ObjectId, ref: 'FacilityInspection', autopopulate: { maxDepth: 2 } }],
  facilitypermits : [{ type: Schema.Types.ObjectId, ref: 'Facilitypermit', autopopulate: { maxDepth: 2 } }],
  facilityemissions : [{ type: Schema.Types.ObjectId, ref: 'Facilityemission', autopopulate: { maxDepth: 2 } }],
  facilityenviprotectfees : [{ type: Schema.Types.ObjectId, ref: 'FacilityEnviProtectFee', autopopulate: { maxDepth: 2 } }],
  facilityenvimonitoringreports : [{ type: Schema.Types.ObjectId, ref: 'FacilityEnviMonitoringReport', autopopulate: { maxDepth: 2 } }],
  facilityhazarwastereports: [{ type: Schema.Types.ObjectId, ref: 'FacilityHazarWasteReport', autopopulate: { maxDepth: 2 } }],
})

var FacilitySimple = new mongoose.Schema({
  ...infoSetting,
  FacilityCode: {type: String, unique: true},
  FacilityID: Number,
  FacilityNameFull: String,
  Latitude: Number,
  Longitude: Number,
  FrequencyOfMonitoring: String, // Tần suất giám sát
  industrialArea: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  province: { type: Schema.Types.ObjectId, ref: 'Province', autopopulate: true },
  sector: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  district: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  authority: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  facilityinspections : [{ type: Schema.Types.ObjectId, ref: 'FacilityInspection', autopopulate: { maxDepth: 2 } }],
  facilitypermits : [{ type: Schema.Types.ObjectId, ref: 'Facilitypermit', autopopulate: { maxDepth: 2 } }],
})

Facility.plugin(autopopulate);

export const FacilitySimpleModel =  mongoose.model('FacilitySimpleModel', FacilitySimple, 'facilities',)
export default mongoose.model('Facility', Facility)
