import mongoose from 'mongoose'
import { infoSetting } from './Base'
import autopopulate from 'mongoose-autopopulate'

const Schema = mongoose.Schema;

var PRTRLoDotConfig = new mongoose.Schema({
  ...infoSetting,
  name: String,
  typeOfWaste: Object,
  levelProcess: Object,
  Description: String,
  measurings: [{ type: Schema.Types.Object, default:null }],
})
PRTRLoDotConfig.plugin(autopopulate);

export default mongoose.model('PRTRLoDotConfig', PRTRLoDotConfig)
