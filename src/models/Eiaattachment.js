import mongoose from 'mongoose'
import { infoSetting } from './Base'
import autopopulate from 'mongoose-autopopulate'
import Attachments from './Attachments'

const Schema = mongoose.Schema;

var Eiaattachment = new mongoose.Schema({
  // ...infoSetting,
  AttachmentID: Number,
  attachment:  { type: Schema.Types.ObjectId, ref: 'Attachment', autopopulate: true },
  EIAID: Number,
  facilityeia:  { type: Schema.Types.ObjectId, ref: 'Facilityeia', autopopulate: true },
})

Eiaattachment.plugin(autopopulate);

export default mongoose.model('Eiaattachment', Eiaattachment)
