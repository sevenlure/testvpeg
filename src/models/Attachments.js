import mongoose from 'mongoose'
import { infoSetting } from './Base'
import autopopulate from 'mongoose-autopopulate'

const Schema = mongoose.Schema;

var Attachment = new mongoose.Schema({
  ...infoSetting,
  AttachmentID: Number,
  AttachmentTypeID: Number,
  attachmentType: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  Title: String,
  FileNameDisplay: String,
  FileNameServer: String,
  NumBytes: Number,
  Description: String,
  isRemove: { type: Boolean, default: false },
})
Attachment.plugin(autopopulate);

export default mongoose.model('Attachment', Attachment)
