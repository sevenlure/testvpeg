import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import { infoSetting } from 'models/Base'
import Facility from './Facility'

const Schema = mongoose.Schema

var FacilityEnviMonitoringReport = new mongoose.Schema({
  ...infoSetting,
  FacilityID: Number,
  facility: {
    type: Schema.Types.ObjectId,
    ref: 'Facility',
    autopopulate: true
  },

  FrequencyOfMonitoring: String,
  RawUsedMaterials: String,
  ProductCapacity: String,
  ParametersList: [{
    Parameter: String,
    Result: String,
    Standard: String,
    Review: String
  }],

  YearOfCharge: Number,
  QuaterOfCharge: String,
  PaymentStatus: String,
  SubmitDate: Date,
  // SubmitTime: String
  // NOTE  Update
  TenCanBoThuLy: String,
  DoiTuong: String,
  TongLLNuocThai: Number,
  TongLLKhiThai: Number,
  BunThai: String,
  NgayNopBaoCao: Date,
  ThoiGianDoDacQuanTrac: Date,
  NgayCoVanBanThongBao: Date,
  SoHieuVanBanTraLoi: String,
  KQQT_NuocThai: String,
  KQQT_KhiThai: String,
  KQQT_BunThai: String
 
})
FacilityEnviMonitoringReport.plugin(autopopulate)

export default mongoose.model('FacilityEnviMonitoringReport', FacilityEnviMonitoringReport)
