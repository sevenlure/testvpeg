import mongoose from 'mongoose'
import { infoSetting } from './Base'
import autopopulate from 'mongoose-autopopulate'

const Schema = mongoose.Schema;

export let PRTRLoDotSchema = new mongoose.Schema({
  ...infoSetting,
  facility:  { type: Schema.Types.ObjectId, ref: 'Facility', autopopulate: true },
  YYYY: { type: Schema.Types.Object, default: null } , 
  MM:{ type: Schema.Types.Object, default: null } ,
  maDN: {type: Schema.Types.String, default: ''},
  typeOfWaste: Object,
  levelProcess: Object,
  config: { type: Schema.Types.ObjectId, ref: 'PRTRLoDotConfig', autopopulate: true },
  name: String,
  Description: String,
  capacityDesign: Number, // Công suốt thiet ke
  capacityOperating: Number, // Công suất hoạt động
  mass: Number, // Khoi luong
  outSite: { type: Schema.Types.Object, default: null },  // cấu hình: tái chế, xử lý
  intSite: { type: Schema.Types.Object, default: null }, // cấu hình: tái chế, xử lý
  khongKhi: [{ type: Schema.Types.Object, default:null }],
  troBay: [{ type: Schema.Types.Object, default:null }],
  xiLo: [{ type: Schema.Types.Object, default:null }],
  isSend: {type: Schema.Types.Boolean, default: false},// xac dinh nhập xong
  sendAt: Date
})
PRTRLoDotSchema.plugin(autopopulate);

export default mongoose.model('PRTRLoDot', PRTRLoDotSchema)
