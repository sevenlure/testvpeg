import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import { infoSetting } from './Base'

const Schema = mongoose.Schema;

const Role = new mongoose.Schema({
  ...infoSetting,
  Name: String,
  Value: String,
  permissionRole: Object
})
Role.plugin(autopopulate);
export default mongoose.model('Role', Role)
