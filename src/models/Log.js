import mongoose from 'mongoose'
import { infoSetting } from './Base'

const EXPIRE_TIME = '60d'

export const TYPE_EVENT = {
  CREATE: 'CREATE',
  UPDATE: 'UPDATE',
  DELETE: 'DELETE',
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
}

const Schema = mongoose.Schema;

const Log = new mongoose.Schema({
  ...infoSetting,
  AddedOn: { type: Date, default: Date.now, expires:EXPIRE_TIME },
  user:  { type: Schema.Types.ObjectId },
  UserName: String,
  TypeEvent: String,
  LogEvent: String
})

export default mongoose.model('Log', Log)
