import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import { PRTRLoDotSchema } from 'models/PRTRLoDot'
import { PRTRNMXLNuocThai } from 'models/PRTRNMXLNuocThai'
const Schema = mongoose.Schema


mongoose.connection.createCollection('viewPRTRFacilities', {
  viewOn: 'facilities',
  pipeline: [
    {
      $lookup: {
        from: 'prtrlodots',
        localField: '_id',
        foreignField: 'facility',
        as: 'prtrlodots_docs'
      }
    },
    { $unwind: { path: '$prtrlodots_docs', preserveNullAndEmptyArrays: true } },
    {
      $lookup: {
        from: 'prtrnmxlnuocthais',
        localField: '_id',
        foreignField: 'facility',
        as: 'prtrNuocThai_docs'
      }
    },
    {
      $unwind: { path: '$prtrNuocThai_docs', preserveNullAndEmptyArrays: true }
    },
    // {
    //   $project: {
    //     FacilityNameFull: 1,
    //     Latitude: 1,
    //     Longitude: 1,
    //     sector:1,
    //     prtrlodots_docs: 1,
    //     prtrNuocThai_docs: 1
    //   }
    // },
    {
      $match: {
        $or: [
          { prtrlodots_docs: { $gte: { $size: 1 } } },
          { prtrNuocThai_docs: { $gte: { $size: 1 } } }
        ]
      }
    }
  ]
})

const View_PRTRSchema = new mongoose.Schema({
  _id: Schema.Types.String,
  FacilityNameFull: Schema.Types.String,
  Latitude: Schema.Types.Number,
  Longitude: Schema.Types.Number,
  sector: {
    type: Schema.Types.ObjectId,
    ref: 'Lookuplistitem',
    autopopulate: true
  },
  prtrlodots_docs: PRTRLoDotSchema,
  prtrNuocThai_docs: PRTRNMXLNuocThai
})
View_PRTRSchema.plugin(autopopulate)

export default mongoose.model(
  'viewPRTRFacilities',
  View_PRTRSchema,
  'viewPRTRFacilities'
)
