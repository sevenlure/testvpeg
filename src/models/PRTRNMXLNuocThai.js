import mongoose from 'mongoose'
import { infoSetting } from './Base'
import autopopulate from 'mongoose-autopopulate'

const Schema = mongoose.Schema;

export let PRTRNMXLNuocThai = new mongoose.Schema({
  ...infoSetting,
  facility:  { type: Schema.Types.ObjectId, ref: 'Facility', autopopulate: true },
  YYYY: { type: Schema.Types.Object, default: null } , 
  MM:{ type: Schema.Types.Object, default: null } ,
  maDN: {type: Schema.Types.String, default: ''},
  capacityDesign: Number, // Công suốt thiet ke
  capacityOperating: Number, // Công suất hoạt động
  nongDoSauXuLy:{type: Schema.Types.Object, default: null},
  sanPhoiBun: Number,
  mayEpBun: Number,
  langTrongLuc: Number,
  mayLyTam: Number,
  xuLyTaiCoSo: Number,
  chuyenGiao: Number,
  khongKhi: [{ type: Schema.Types.Object, default:null }],
  nuoc: [{ type: Schema.Types.Object, default:null }],
  isSend: {type: Schema.Types.Boolean, default: false},// xac dinh nhập xong
  sendAt: Date, 
  donViPhanTich: String,
  ngayLayMau: Date,
  ngayTraKetQua: Date,
  isMap: Boolean
})
PRTRNMXLNuocThai.plugin(autopopulate);

export default mongoose.model('PRTRNMXLNuocThai', PRTRNMXLNuocThai)
