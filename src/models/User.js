import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import { infoSetting } from './Base'

const Schema = mongoose.Schema;

const User = new mongoose.Schema({
  ...infoSetting,
  UserID: Number,
  ProvinceID: Number,
  DistrictID: Number,
  district: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  UserName: String,
  StatusCode: Number,
  SecurityLevel: Number,
  securityLevel: String,//Schema.Types.ObjectId,
  IsSupport: Boolean,
  FirstName: String,
  LastName: String,
  Email: String,
  Telephone: String,
  Fax: String,
  Password: String,
  Organization: String,
  OrganizationUnit: String,
  LastAccessed: Date,
  NumberOfLogins: Number,
  PasswordResetKey: String,
  sector: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  role: { type: Schema.Types.ObjectId, ref: 'Role', autopopulate: true },
  facility:{ type: Schema.Types.ObjectId, ref: 'Facility', autopopulate: true },
  TokenLast: Object
})
User.plugin(autopopulate);
export default mongoose.model('User', User)
