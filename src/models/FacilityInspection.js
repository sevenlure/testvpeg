import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import { infoSetting } from './Base'
import Facility from './Facility'

const Schema = mongoose.Schema;

var FacilityInspection = new mongoose.Schema({
    ...infoSetting,
    InspectionID: Number,
    FacilityID: Number,
    facility: { type: Schema.Types.ObjectId, ref: 'Facility', autopopulate: { maxDepth: 2 }  },
    InspectionDate: { type: Date, default: Date.now },
    InspectionStatusID: Number,//Thanh tra Tình trạng
    inspectionStatus: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
    Purpose: String,
    PurposeID: Number,// Muc dich
    purposeObj: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true }, //de phan biet voi filed Purpose
    Notes: String,
    PlanURL: String,
    ReportURL: String,
    Team: String,
    StartTime: Date,
    EndTime: Date,
    SamplesTaken: Boolean,
    ComplianceTypeID: Number, //Su kien tuan thu
    complianceType: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
    complianceFile: Object,
    ComplianceFine: Number, //Tien phat
})
FacilityInspection.plugin(autopopulate);

var FacilityInspectionSimple = new mongoose.Schema({
  ...infoSetting,
  facility: { type: Schema.Types.ObjectId, ref: 'Facility', autopopulate: { maxDepth: 2 }  },
  InspectionDate: { type: Date, default: Date.now },
  inspectionStatus: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  Purpose: String,
  PurposeID: Number,// Muc dich
  purposeObj: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true }, //de phan biet voi filed Purpose
  complianceType: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  complianceFile: Object,
  ComplianceFine: Number, //Tien phat
})

var FacilityInspectionBCTK = new mongoose.Schema({
  ...infoSetting,
  facility: { type: Schema.Types.ObjectId, ref: 'Facility',autopopulate: { maxDepth: 1 }  },
  InspectionDate: { type: Date, default: Date.now },
  inspectionStatus: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: false },
  Purpose: String,
  PurposeID: Number,// Muc dich
  purposeObj: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: false }, //de phan biet voi filed Purpose
  complianceType: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: false },
  complianceFile: Object,
  ComplianceFine: Number, //Tien phat
})
FacilityInspectionBCTK.plugin(autopopulate);


export const FacilityInspectionSimpleModel =  mongoose.model('FacilityInspectionSimpleModel', FacilityInspectionSimple, 'facilityinspections',)
export const FacilityInspectionBCTKModel =  mongoose.model('FacilityInspectionBCTKModel', FacilityInspectionBCTK, 'facilityinspections',)
export default mongoose.model('FacilityInspection', FacilityInspection, 'facilityinspections')
