import mongoose from 'mongoose'
import { infoSetting } from './Base'
import autopopulate from 'mongoose-autopopulate'
import Attachments from './Attachments'

const Schema = mongoose.Schema;

var Permitattachment = new mongoose.Schema({
  // ...infoSetting,
  AttachmentID: Number,
  attachment:  { type: Schema.Types.ObjectId, ref: 'Attachment', autopopulate: true },
  PermitID: Number,
  facilitypermit:  { type: Schema.Types.ObjectId, ref: 'Facilitypermit', autopopulate: true },
})

Permitattachment.plugin(autopopulate);

export default mongoose.model('Permitattachment', Permitattachment)
