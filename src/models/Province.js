import mongoose from 'mongoose'
import { infoSetting } from './Base'

const Province = new mongoose.Schema({
  ...infoSetting,
  ProvinceID: Number,
  NameLong: String,
  NameShort: String,
  CentreLat: Number,
  CentreLong: Number,
  DefaultScale: Number,
})

export default mongoose.model('Province', Province)
