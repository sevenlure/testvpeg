import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import { infoSetting } from 'models/Base'
import Facility from './Facility'

const Schema = mongoose.Schema

var Facilitypermit = new mongoose.Schema({
  ...infoSetting,
  PermitID: Number,
  FacilityID: Number,
  facility: {
    type: Schema.Types.ObjectId,
    ref: 'Facility',
    autopopulate: true
  },
  PermitTypeID: Number,
  permitType: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  IssueDate:Date,
  ExpiryDate: Date,
  PermitURL: String,
  Notes: String,
})
Facilitypermit.plugin(autopopulate)

var FacilitypermitSimple = new mongoose.Schema({
  ...infoSetting,
  facility: {
    type: Schema.Types.ObjectId,
    ref: 'Facility',
    autopopulate: true
  },
  permitType: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  IssueDate:Date,
})

export const FacilitypermitSimpleModel =  mongoose.model('FacilitypermitSimpleModel', FacilitypermitSimple, 'facilitypermits',)

export default mongoose.model('Facilitypermit', Facilitypermit)
