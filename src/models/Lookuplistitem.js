import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import Lookuplist from './Lookuplist'
import { infoSetting } from './Base'

const Schema = mongoose.Schema;

var Lookuplistitem = new mongoose.Schema({
  ...infoSetting,
  ListItemID: Number,
  ProvinceID: Number,
  LookupListID: Number,
  lookuplist: { type: Schema.Types.ObjectId, ref: 'Lookuplist', autopopulate: true },
  Key: String,
  Name: String,
  ExtraInfo: String,
  DisplaySequence: Number,
  OldID: Number,
  PRTRKey: String,
  IsEditableByUser: Boolean,
  IsAvailableForNewItems: Boolean,
  children: Array,
  Options: Object, // nganh nghe nao su dung hoa chat
})
Lookuplistitem.plugin(autopopulate);

export default mongoose.model('Lookuplistitem', Lookuplistitem)
