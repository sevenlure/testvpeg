import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import { infoSetting } from 'models/Base'
import Facility from './Facility'

const Schema = mongoose.Schema

var Facilityemission = new mongoose.Schema({
  ...infoSetting,
  EmissionID: Number,
  FacilityID: Number,
  facility: {
    type: Schema.Types.ObjectId,
    ref: 'FacilityOfEmission',
    autopopulate: true
  },
  EmissionTypeID: Number,
  emissionType: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  Name: String,
  DataValue: Number,
  Description: String,
})
Facilityemission.plugin(autopopulate)


var FacilityemissionSimple = new mongoose.Schema({
  ...infoSetting,
  facility: {
    type: Schema.Types.ObjectId,
    ref: 'FacilityOfEmission',
    autopopulate: true
  },
  emissionType: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  Name: String,
  DataValue: Number,
})

var FacilityOfEmission = new mongoose.Schema({
  FacilityNameFull: String,
  industrialArea: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  sector: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  district: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
  // authority: { type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true },
})
FacilityOfEmission.plugin(autopopulate)
export const FacilityOfEmissionModel =  mongoose.model('FacilityOfEmission', FacilityOfEmission, 'facilities',)


export const FacilityemissionSimpleModel =  mongoose.model('FacilityemissionSimpleModel', FacilityemissionSimple, 'facilityemissions',)

export default mongoose.model('Facilityemission', Facilityemission)
