import mongoose from 'mongoose'
import { infoSetting } from './Base'
import autopopulate from 'mongoose-autopopulate'
import Attachments from './Attachments'

const Schema = mongoose.Schema;

var Facilityattachment = new mongoose.Schema({
  // ...infoSetting,
  AttachmentID: Number,
  attachment:  { type: Schema.Types.ObjectId, ref: 'Attachment', autopopulate: true },
  FacilityID: Number,
  facility:  { type: Schema.Types.ObjectId, ref: 'Facility', autopopulate: true },
})

Facilityattachment.plugin(autopopulate);

export default mongoose.model('Facilityattachment', Facilityattachment)
