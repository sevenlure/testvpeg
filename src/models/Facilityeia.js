import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import { infoSetting } from 'models/Base'
import Facility from './Facility'

const Schema = mongoose.Schema

var Facilityeia = new mongoose.Schema({
  ...infoSetting,
  EIAID: Number,
  FacilityID: Number,
  facility: {
    type: Schema.Types.ObjectId,
    ref: 'Facility',
    autopopulate: true
  },
  Title: String,//{ type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true }, Quan trong
  DateIssued: Date,
  EIAURL: String,
  Remarks: String,
  DecisionNumber: String, // Số Quyết Định
  IssuingAgency: String, // CƠ QUAN CẤP
  IsCompleteDTM: { type: Boolean, default: false }, // thuọc doi tuong hoan thjanh DTM
  IsEnvironmentalIncident: { type: Boolean, default: false } //thuộc sự cố môi trường
})
Facilityeia.plugin(autopopulate)


var FacilityeiaBCTK = new mongoose.Schema({
  ...infoSetting,
  DateIssued: Date,
  facility: { type: Schema.Types.ObjectId, ref: 'Facility',autopopulate: { maxDepth: 1 }  },
  IsCompleteDTM: { type: Boolean, default: false }, // thuọc doi tuong hoan thjanh DTM
  IsEnvironmentalIncident: { type: Boolean, default: false }, //thuộc sự cố môi trường
  Title: String,//{ type: Schema.Types.ObjectId, ref: 'Lookuplistitem', autopopulate: true }, Quan trong
})
FacilityeiaBCTK.plugin(autopopulate);


export const FacilityeiaBCTKModel =  mongoose.model('FacilityeiaBCTKModel', FacilityeiaBCTK, 'facilityeias',)
export default mongoose.model('Facilityeia', Facilityeia)
