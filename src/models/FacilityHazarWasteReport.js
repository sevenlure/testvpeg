import mongoose from 'mongoose'
import autopopulate from 'mongoose-autopopulate'
import { infoSetting } from 'models/Base'
import Facility from './Facility'

const Schema = mongoose.Schema

var FacilityHazarWasteReport = new mongoose.Schema({
  ...infoSetting,
  FacilityID: Number,
  facility: {
    type: Schema.Types.ObjectId,
    ref: 'Facility',
    autopopulate: true
  },

  // Collector: String,
  collector: {
    type: Schema.Types.ObjectId,
    ref: 'Lookuplistitem',
    autopopulate: true
  },
  ExtraInfo: String,
  Volume: Number,

  CodeQLCNT: String,
  DateIssued: Date,
  Year: Number,
  SubmitDate: Date, //Ngày nộp
  PaymentStatus: String, //Tinh Trang nop
  ReplyDate: Date, //Ngay tra loi
  VanBanPhatHanh: String,
  SumGiaiQuyetDate: Number, // tinh theo ngay

  Waste_Volume: Number, //Tong khoi luong  kg/nam
  Waste_Sludge: Number, //bùn thải kg.nam
  Waste_Process_Unit: String, //Don vi xu ly
  Waste_TrongNgoaiTinh: String,

  // NOTE  cap nhat
  MaSoQLCTNH: String, // Tên cán bộ thụ lý hồ sơ
  CapLan: String,
  NamThucHienBaoCao: Number,
  HinhThucNop: String,
  NgayNopBaoCao: Date,
  TenCanBoThuLy: String,
  NgayCoYKienTraLoi: Date,
  SoHieuVanBanTraLoi: String,
  TongKhoiLuongCTCNTT: Number,
  TongKhoiLuongCTCNNH: Number,
  TongKhoiLuongBunNguyHai: Number,
  DonViThuGom_CTCNTT: {
    type: Schema.Types.ObjectId,
    ref: 'Lookuplistitem',
    autopopulate: true
  },
  DonViThuGom_CTCNNH: {
    type: Schema.Types.ObjectId,
    ref: 'Lookuplistitem',
    autopopulate: true
  }
})
FacilityHazarWasteReport.plugin(autopopulate)

export default mongoose.model(
  'FacilityHazarWasteReport',
  FacilityHazarWasteReport
)
