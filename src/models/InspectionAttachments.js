import mongoose from 'mongoose'
// import Facilityinspection from 'models/FacilityInspection'
import autopopulate from 'mongoose-autopopulate'
import Attachments from './Attachments'

const Schema = mongoose.Schema;

var InspectionAttachment = new mongoose.Schema({
  AttachmentID: Number,
  attachment: { type: Schema.Types.ObjectId, ref: 'Attachment', autopopulate: true },
  InspectionID: Number,
  inspection: { type: Schema.Types.ObjectId, ref: 'FacilityInspection', autopopulate: true },
})

InspectionAttachment.plugin(autopopulate);

export default mongoose.model('InspectionAttachment', InspectionAttachment)
