export const infoSetting = {
  AddedOn: { type: Date, default: Date.now },
  AddedBy: String,
  UpdatedOn: { type: Date, default: Date.now },
  UpdatedBy: String
}
