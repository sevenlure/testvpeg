export function resError (res, messageError, typeError = '') {
  res.json({ error: true, message: messageError, typeError })
}
export function resSuccess (res, object = {}, success = true) {
  res.json(Object.assign(object, { success }))
}
