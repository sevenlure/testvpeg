let multer = require('multer')
let fs = require('fs')
import path from 'path'
import { FOLDER_UPLOAD } from '../config'

function getFileType(fileName) {
  const strs = fileName.split('.')
  return strs[strs.length - 1]
}

function cleanOriginalName(fileName) {
  const strs = fileName.split('.')
  return strs[0]
}

function createMulter(customPath = '/') {
  if (!fs.existsSync(path.join(FOLDER_UPLOAD))) {
    fs.mkdirSync(path.join(FOLDER_UPLOAD))
  }
  if (!fs.existsSync(path.join(FOLDER_UPLOAD, customPath))) {
    fs.mkdirSync(path.join(FOLDER_UPLOAD, customPath))
  }
  const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, path.join(FOLDER_UPLOAD, customPath))
    },
    filename: function(req, file, cb) {
      let fileName = `${cleanOriginalName(
        file.originalname
      )}_${Date.now()}.${getFileType(file.originalname)}`
      cb(null, fileName)
    }
  })
  return multer({ storage })
}

export default createMulter
