import jimp from 'jimp'

export function resizeImageTo1024(path, cb) {
  jimp.read(path, function(err, image) {
    if (err) {
      console.log(err)
    }
    if (image.bitmap.width > 1024) {
      image.scaleToFit(1024, jimp.AUTO).write(path)
      setTimeout(() => {
        cb()
      }, 10)
    } else {
      cb()
    }
  })
}
// Lib
