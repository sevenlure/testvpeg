import {
  borderStyle as borderStyleBase,
  headerStyle as headerStyleBase,
  CellFirstStyle as CellFirstStyleBase,
  CellSecondStyle as CellSecondStyleBase
} from './base'

const borderStyle = {
  ...borderStyleBase
}
const headerStyle = {
  ...headerStyleBase
}

const CellFirstStyle = {
  ...CellFirstStyleBase,
  border: borderStyle
}
const CellSecondStyle = {
  ...CellSecondStyleBase,
  border: borderStyle
}

export const cellStyle = function(value, row) {
  return row.index % 2 == 1 ? CellFirstStyle : CellSecondStyle
}

const specification = {
  FacilityNameFull: {
    displayName: 'Tên Cơ Sở', // <- Here you specify the column header
    headerStyle: headerStyle, // <- Header style
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          vertical: 'center',
          horizontal: 'left',
          wrapText: true
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.FacilityNameFull', '')
    },
    width: 386 // <- width in pixels
  },
  industrialArea: {
    displayName: 'Khu công nghiệp/Cụm Công nghiệp',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.industrialArea.Name', '')
    },
    width: 315
  },
  district: {
    displayName: 'Quận/Huyện',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.district.Name', '')
    },
    width: 257 // <- width in pixels
  },
  sector: {
    displayName: 'Ngành nghề',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.sector.Name', '')
    },
    width: 310 // <- width in pixels
  },
  InspectionDate: {
    displayName: 'Ngày',
    headerStyle: headerStyle,
    cellStyle,
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return value ? moment(value).format(DATE_FORMAT) : ''
    },
    width: 110 // <- width in pixels
  },
  inspectionStatus: {
    displayName: 'Tình trạng',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(value, 'Name', '')
    },
    width: 110 // <- width in pixels
  },
  purposeObj: {
    displayName: 'Mục đích',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(value, 'Name', '')
    },
    width: 180 // <- width in pixels
  },
  complianceType: {
    displayName: 'Sự kiện Tuân thủ',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(value, 'Name', '')
    },
    width: 150 // <- width in pixels
  },
  ComplianceFine: {
    displayName: 'Số tiền (đ)',
    headerStyle: headerStyle,
    cellStyle,
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return value ? value : ''
    },
    width: 150 // <- width in pixels
  },
  AddedOn: {
    displayName: 'Thêm vào',
    cellStyle,
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return value ? moment(value).format(DATE_FORMAT) : ''
    },
    headerStyle: headerStyle,
    width: 175 // <- width in pixels
  },
  UpdatedOn: {
    displayName: 'Cập nhật ngày',
    cellStyle,
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return value ? moment(value).format(DATE_FORMAT) : ''
    },
    headerStyle: headerStyle,
    width: 175 // <- width in pixels
  }
}

export default specification
