import {
  borderStyle as borderStyleBase,
  headerStyle as headerStyleBase,
  CellFirstStyle as CellFirstStyleBase,
  CellSecondStyle as CellSecondStyleBase,
} from './base'

const borderStyle = {
  ...borderStyleBase
}
const headerStyle = {
  ...headerStyleBase
}

const CellFirstStyle = {
  ...CellFirstStyleBase,
  border: borderStyle
}
const CellSecondStyle = {
  ...CellSecondStyleBase,
  border: borderStyle
}

export const cellStyle = function(value, row) {
  return row.index % 2 == 1 ? CellFirstStyle : CellSecondStyle
}

const specification = {
  FacilityNameFull: {
    displayName: 'Tên Cơ Sở', // <- Here you specify the column header
    headerStyle: headerStyle, // <- Header style
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          vertical: 'center',
          horizontal: 'left',
          wrapText: true
        }
      }
    },
    width: 386 // <- width in pixels
  },
  industrialArea: {
    displayName: 'Khu công nghiệp/Cụm Công nghiệp',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return value ? value.Name : ''
    },
    width: 315
  },
  district: {
    displayName: 'Quận/Huyện',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return value ? value.Name : ''
    },
    width: 257 // <- width in pixels
  },
  sector: {
    displayName: 'Ngành nghề',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return value ? value.Name : ''
    },
    width: 310 // <- width in pixels
  },
  authority: {
    displayName: 'Thẩm quyền quản lý môi trường',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return value ? value.Name : ''
    },
    width: 292 // <- width in pixels
  },
  facilityinspections: {
    displayName: 'Thanh tra',
    headerStyle: headerStyle,
    cellFormat: function(value, row) {
      return value ? value.length : 0
    },
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        numFmt: '0',
      }
    },
    width: 140 // <- width in pixels
  },
  facilitypermits: {
    displayName: 'Giấy phép',
    headerStyle: headerStyle,
    cellFormat: function(value, row) {
      return value ? value.length : 0
    },
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        numFmt: '0'
      }
    },
    width: 140 // <- width in pixels
  },
  AddedOn: {
    displayName: 'Thêm vào',
    cellStyle,
    headerStyle: headerStyle,
    width: 175 // <- width in pixels
  },
  UpdatedOn: {
    displayName: 'Cập nhật ngày',
    cellStyle,
    headerStyle: headerStyle,
    width: 175 // <- width in pixels
  }
}

export default specification
