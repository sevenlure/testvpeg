import {
  borderStyle as borderStyleBase,
  headerStyle as headerStyleBase,
  CellFirstStyle as CellFirstStyleBase,
  CellSecondStyle as CellSecondStyleBase
} from './base'
import he from 'he'

const borderStyle = {
  ...borderStyleBase
}
const headerStyle = {
  ...headerStyleBase
}

const CellFirstStyle = {
  ...CellFirstStyleBase,
  border: borderStyle
}
const CellSecondStyle = {
  ...CellSecondStyleBase,
  border: borderStyle
}

export const cellStyle = function(value, row) {
  return row.index % 2 == 1 ? CellFirstStyle : CellSecondStyle
}

const specification = {
  FacilityNameFull: {
    displayName: 'Tên Cơ Sở', // <- Here you specify the column header
    headerStyle: headerStyle, // <- Header style
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          vertical: 'center',
          horizontal: 'left',
          wrapText: true
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.FacilityNameFull', '')
    },
    width: 386 // <- width in pixels
  },
  industrialArea: {
    displayName: 'Khu công nghiệp/Cụm Công nghiệp',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.industrialArea.Name', '')
    },
    width: 315
  },
  district: {
    displayName: 'Quận/Huyện',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.district.Name', '')
    },
    width: 257 // <- width in pixels
  },
  sector: {
    displayName: 'Ngành nghề',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.sector.Name', '')
    },
    width: 310 // <- width in pixels
  },

  //Địa chỉ phát sinh nước thải
  IncurredAddress: {
    displayName: 'Địa chỉ phát sinh nước thải',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    width: 310 // <- width in pixels
  },

  YearOfCharge: {
    displayName: 'Năm tính phí',
    headerStyle: headerStyle,
    cellStyle,
    cellFormat: function(value, row) {
      return value ? value:''
    },
    width: 110 // <- width in pixels
  },

  QuaterOfCharge: {
    displayName: 'Quý tính phí',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    width: 110 // <- width in pixels
  },

  SubjectOfCharge: {
    displayName: 'Thuộc đối tượng',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    width: 110 // <- width in pixels
  },

  AverageWaterDemand: {
    displayName: 'Nhu cầu sử dụng nước',
    headerStyle: headerStyle,
    cellStyle,
    cellFormat: function(value, row) {
      return value ? value:''
    },
    width: 110 // <- width in pixels
  },

  AverageWasteWater: {
    displayName: 'Lưu lượng nước thải phát sinh trung bình',
    headerStyle: headerStyle,
    cellStyle,
    cellFormat: function(value, row) {
      return value ? value:''
    },
    width: 310 // <- width in pixels
  },
  AverageWasteWaterEnviProfile: {
    displayName: 'Lượng nước thải phát sinh theo hồ sơ môi trường',
    headerStyle: headerStyle,
    cellStyle,
    cellFormat: function(value, row) {
      return value ? value:''
    },
    width: 310 // <- width in pixels
  },

  AverageWasteWaterActual: {
    displayName: 'Lượng nước thải phát sinh theo thực tế',
    headerStyle: headerStyle,
    cellStyle,
    cellFormat: function(value, row) {
      return value ? value:''
    },
    width: 310 // <- width in pixels
  },

  TotalWasteWaterOfQuarter: {
    displayName: 'Tổng lượng nước thải phát sinh trong quý',
    headerStyle: headerStyle,
    cellStyle,
    cellFormat: function(value, row) {
      return value ? value:''
    },
    width: 310 // <- width in pixels
  },

  AddedOn: {
    displayName: 'Thêm vào',
    cellStyle,
    headerStyle: headerStyle,
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return value ? moment(value).format(DATE_FORMAT) : ''
    },
    width: 175 // <- width in pixels
  },
  UpdatedOn: {
    displayName: 'Cập nhật ngày',
    cellStyle,
    headerStyle: headerStyle,
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return value ? moment(value).format(DATE_FORMAT) : ''
    },
    width: 175 // <- width in pixels
  }
}

export default specification

// m³/ ngày
