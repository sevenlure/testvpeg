export const borderStyle= {
  top: {
    style: 'thin',
    color:{ rgb: "009BBB59" }
  },
  bottom: {
    style: 'thin',
    color:{ rgb: "009BBB59" }
  },
  left: {
    style: 'thin',
    color: { rgb: "009BBB59" }
  },
  right: {
    style: 'thin',
    color: { rgb: "009BBB59" }
  }
}
export const headerStyle={
  alignment:{
    vertical: "top",
    horizontal: "center"
  },
  font: {
    bold: true,
    name: 'Times New Roman',
    sz: '11',
    color:{ rgb: "00FFFFFF" }
  },
  fill:{
    bgColor:{ rgb: "009BBB59" },
    fgColor: { rgb: "009BBB59" }
  }
}

export const CellFirstStyle ={
  alignment:{
    vertical: "center",
    horizontal: "center"
  },
  font: {
    name: 'Times New Roman',
    sz: '11',
    color:{ rgb: "0000000" }
  },
  fill:{
    bgColor:{ rgb: "00EBF1DE" },
    fgColor: { rgb: "00EBF1DE" }
  },
  border: borderStyle
}
export const CellSecondStyle ={
  alignment:{
    vertical: "center",
    horizontal: "center"
  },
  font: {
    name: 'Times New Roman',
    sz: '11',
    color:{ rgb: "0000000" }
  },
  fill:{
    bgColor:{ rgb: "00FFFFFF" },
    fgColor: { rgb: "00FFFFFF" }
  },
  border: borderStyle
}


export const cellStyle=  function(value, row) {
  return (row.index%2==1)? CellFirstStyle:CellSecondStyle
}