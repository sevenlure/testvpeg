import {
  borderStyle as borderStyleBase,
  headerStyle as headerStyleBase,
  CellFirstStyle as CellFirstStyleBase,
  CellSecondStyle as CellSecondStyleBase
} from './base'
import he from 'he'

const borderStyle = {
  ...borderStyleBase
}
const headerStyle = {
  ...headerStyleBase
}

const CellFirstStyle = {
  ...CellFirstStyleBase,
  border: borderStyle
}
const CellSecondStyle = {
  ...CellSecondStyleBase,
  border: borderStyle
}

export const cellStyle = function(value, row) {
  return row.index % 2 == 1 ? CellFirstStyle : CellSecondStyle
}

const specification = {
  FacilityNameFull: {
    displayName: 'Tên Cơ Sở', // <- Here you specify the column header
    headerStyle: headerStyle, // <- Header style
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          vertical: 'center',
          horizontal: 'left',
          wrapText: true
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.FacilityNameFull', '')
    },
    width: 386 // <- width in pixels
  },
  industrialArea: {
    displayName: 'Khu công nghiệp/Cụm Công nghiệp',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.industrialArea.Name', '')
    },
    width: 315
  },
  district: {
    displayName: 'Quận/Huyện',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.district.Name', '')
    },
    width: 257 // <- width in pixels
  },
  sector: {
    displayName: 'Ngành nghề',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      // <- Renderer function, you can access also any row.property
      return _.result(row, 'facility.sector.Name', '')
    },
    width: 310 // <- width in pixels
  },

  Title: {
    displayName: 'Loại hồ sơ',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      return value ? value:''
    },
    width: 210 // <- width in pixels
  },
 
  DecisionNumber: {
    displayName: 'Số quyết định',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      return value ? value:''
    },
    width: 210 // <- width in pixels
  },

  DateIssued: {
    displayName: 'Ngày phát hành',
    cellStyle,
    headerStyle: headerStyle,
    width: 175 // <- width in pixels
  },

  IssuingAgency: {
    displayName: 'Cơ quan cấp',
    headerStyle: headerStyle,
    cellStyle: function(value, row) {
      return {
        ...cellStyle(value, row),
        alignment: {
          horizontal: 'left',
        }
      }
    },
    cellFormat: function(value, row) {
      return value ? value:''
    },
    width: 210 // <- width in pixels
  },

  IsCompleteDTM: {
    displayName: 'Thuộc đối tượng lập hồ sơ xác nhận hoàn thành ĐTM',
    headerStyle: headerStyle,
    cellStyle,
    cellFormat: function(value, row) {
      return value ? 'Có':'Không'
    },
    width: 210 // <- width in pixels
  },

  IsEnvironmentalIncident: {
    displayName: 'Thuộc đối tượng lập phương án ứng phó sự cố môi trường',
    headerStyle: headerStyle,
    cellStyle,
    cellFormat: function(value, row) {
      return value ? 'Có':'Không'
    },
    width: 210 // <- width in pixels
  },

  AddedOn: {
    displayName: 'Thêm vào',
    cellStyle,
    headerStyle: headerStyle,
    width: 175 // <- width in pixels
  },
  UpdatedOn: {
    displayName: 'Cập nhật ngày',
    cellStyle,
    headerStyle: headerStyle,
    width: 175 // <- width in pixels
  }
}

export default specification

// m³/ ngày
