export const PORT = 3005
export const DATABASE_NAME = 'VPEG'

export const DATE_FORMAT = 'DD/MM/YYYY'
export const BLACKLIST_JSON_PATH = '/blackListToken'

export const EMAIL_USER_SYSTEM = 'VPEG.SYSTEM@gmail.com'
export const EMAIL_TOKEN_SYSTEM = 'sekdrhptwzgazjwb'
export const EMAIL_ALERT_LOGIN = 'quanlycsdlbinhduong@gmail.com'

export const MONGODB_OPTIONS = {
  database: `mongodb://localhost:27017/${DATABASE_NAME}`,
  opt: {
    // user: 'dev',
    // pass: '123456',
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    autoReconnect: true,
    useNewUrlParser: true
  }
}

export const JWT_SECRET = 'VPEG_JWTSECRET'
// export const AUTH_API = 'http://localhost:1234'
export const PUBLIC_URL = 'http://stnmt.binhduong.gov.vn:3005'
export const FOLDER_UPLOAD = 'public/FileDatabase'

export default {
  PORT,
  MONGODB_OPTIONS,
  JWT_SECRET,
  // AUTH_API
}
