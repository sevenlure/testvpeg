export default function(sequelize, DataTypes) {
  return sequelize.define(
    'DiemDo',
    {
      OBJECTID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      MaDiem: DataTypes.STRING,
      TenDiem: DataTypes.STRING,
    },
    {
      tableName: 'DiemDo',
      timestamps: false,
      freezeTableName: true
    }
  )
}
