// import Sequelize from 'sequelize'
// Sequelize.NUMERIC
export default function(sequelize, DataTypes) {
  return sequelize.define(
    'SoLieu_MoiNhat',
    {
      OBJECTID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      MaDiem: DataTypes.STRING,
      MaThongSo: DataTypes.STRING,
      ThoiGian: DataTypes.DATE,
      GiaTri: DataTypes.NUMERIC
    },
    {
      tableName: 'SoLieu_MoiNhat',
      timestamps: false,
      freezeTableName: true
    }
  )
}

