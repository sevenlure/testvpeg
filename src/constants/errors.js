export default {
  // USER_PASSWORD_INCORRECT: 'USER_PASSWORD_INCORRECT',
  USER_PASSWORD_INCORRECT: 'The UserName or Password is incorrect.',
  USER_REGISTER_EXISTS: 'USER_REGISTER_EXISTS',
  NOT_AUTHENTICATED: 'Lỗi chứng thực !!!',
  TOKEN_EXPIRE: 'Hết Phiên làm việc',
  USER_OR_ROLE_CHANGED: 'Tài khoản có sử thay đổi, Cần đăng nhập lại.'
}