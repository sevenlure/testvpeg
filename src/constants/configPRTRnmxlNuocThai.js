export const dumpData_config = {
    success: true,
    data: [
      {
        key: 'ammonia',
        name: 'Ammonia (total)',
        value: 34.6,
        fator: 1000,
        Recipe: {
          congSuat: true
        },
        Enviroment: {
          khi: 2.2
        }
      },
      {
        key: 'total_VOCs',
        name: 'Total volatile organic compounds',
        fator: 1000,
        Recipe: {
          congSuat: true
        },
        Enviroment: {
          khi: 1.07
        }
      },
      {
        key: 'acrolein',
        name: 'Acrolein',
        fator: 1000,
        Enviroment: {
          khi: 0.18
        }
      },
      {
        key: 'acrylonitrile',
        name: 'Acrylonitrile',
        fator: 1000,
        Enviroment: {
          khi: 0.24
        }
      },
      {
        key: 'aniline',
        name: 'Aniline',
        fator: 1000,
        Enviroment: {
          khi: 1.2
        }
      },
      {
        key: 'benzene',
        name: 'Benzene',
        fator: 1000,
        Enviroment: {
          khi: 1.14,
          giamAmBun: {
            sanPhoiBun: 0.34,
            mayEpBun: 6,
            langTrongLuc: null,
            mayLyTam: 0.22
          }
        }
      },
      {
        key: 'benzene_hexachloro',
        name: 'Benzene hexachloro (HCB)',
        fator: 1000,
        Enviroment: {
          khi: 0.52
        }
      },
      {
        key: 'butadiene',
        name: '1,3- Butadiene',
        fator: 1000,
        Enviroment: {
          khi: 1.7
        }
      },
      {
        key: 'chloroform',
        name: 'Chloroform',
        fator: 1000,
        Enviroment: {
          khi: 1.04,
          giamAmBun: {
            mayEpBun: 1.32,
            mayLyTam: 0.67,
          }
        }
      },
      {
        key: 'chlorophenols',
        name: 'Chlorophenols',
        fator: 1000,
        Enviroment: {
          khi: 0.03
        }
      },
      {
        key: 'dichloromethane',
        name: 'Dichloromethane',
        fator: 1000,
        Enviroment: {
          khi: 1.04,
          giamAmBun: {
            sanPhoiBun: 0.68,
            mayEpBun: 1.64,
            langTrongLuc: 1.57,
            mayLyTam: 0.64
          }
        }
      },
      {
        key: 'formaldehyde',
        name: 'Formaldehyde',
        fator: 1000,
        Enviroment: {
          giamAmBun: {
            sanPhoiBun: 0.14,
            mayEpBun: 7.96,
            langTrongLuc: 6e-3,
            mayLyTam: 1.36
          }
        }
      },
      {
        key: 'ethylbenzene',
        name: 'Ethylbenzene',
        fator: 1000,
        Enviroment: {
          khi: 1.12
        }
      },
      {
        key: 'arsenic_compounds',
        name: 'Arsenic & compounds',
        value: 0.002,
        fator: 1000,
      },
      {
        key: 'boron_compounds',
        name: 'Boron & compounds',
        value: 0.221,
        fator:1000,
      },
      {
        key: 'cadmium_compounds',
        name: 'Cadmium & compounds',
        value: 0.0005,
        fator:1000,
      },
      {
        key: 'chromium',
        name: 'Chromium (total)',
        value: 0.011,
        fator:1000,
      },
      {
        key: 'cobalt_compounds',
        name: 'Cobalt & compounds',
        value: 0.0013,
        fator:1000,
      },
      {
        key: 'copper_compounds',
        name: 'Copper & compounds',
        value: 0.098,
        fator:1000,
      },
      {
        key: 'fluoride_compounds',
        name: 'Fluoride compounds',
        value: 0.93,
        fator:1000,
      },
      {
        key: 'lead_compounds',
        name: 'Lead & compounds',
        value: 0.0002,
        fator:1000,
      },
      {
        key: 'manganese_compounds',
        name: 'Manganese & compounds',
        value: 0.177,
        fator:1000,
      },
      {
        key: 'mercury_compounds',
        name: 'Mercury & compounds',
        value: 0.0002,
        fator:1000,
      },
      {
        key: 'nickel_compounds',
        name: 'Nickel & compounds',
        value: 0.011,
        fator:1000,
      },
      {
        key: 'phenol',
        name: 'Phenol',
        value: 0.051,
        fator: 1000,
        Enviroment: {
          khi: 0.0018
        }
      },
      {
        key: 'styrene',
        name: 'Styrene',
        fator: 1000,
        Enviroment: {
          khi: 0.5,
          giamAmBun: {
            langTrongLuc: 1.44e-3,
            mayLyTam: 0.37
          }
        }
      },
      {
        key: 'Toluene',
        name: 'Toluene',
        fator: 1000,
        Enviroment: {
          khi: 1.16,
          giamAmBun: {
            sanPhoiBun: 0.16,
            mayEpBun: 6.45,
            langTrongLuc: 0.33,
            mayLyTam: 213.3
          }
        }
      },
      {
        key: 'Trichloroethylene',
        name: 'Trichloroethylene',
        fator: 1000,
        Enviroment: {
          khi: 1.24,
          giamAmBun: {
            sanPhoiBun: null,
            mayEpBun: 11.8,
            langTrongLuc: 0.42,
            mayLyTam: 0.2
          }
        }
      },
      {
        key: 'Vinyl_chloride_monomer',
        name: 'Vinyl chloride monomer',
        fator: 1000,
        Enviroment: {
          khi: 1.62,
          giamAmBun: {
            sanPhoiBun: null,
            mayEpBun: null,
            langTrongLuc: null,
            mayLyTam: 0.56
          }
        }
      },
      {
        key: 'Xylene',
        name: 'Xylene',
        fator: 1000,
        Enviroment: {
          khi: 1.1,
          giamAmBun: {
            sanPhoiBun: 1.32,
            mayEpBun: 3.4,
            langTrongLuc: 0.15,
            mayLyTam: 1.58
          }
        }
      },
      {
        key: 'selenium_compounds',
        name: 'Selenium & compounds',
        value: 0.0043,
        fator: 1000,
      },
      {
        key: 'total_nitrogen',
        name: 'Total Nitrogen',
        value: 60.96,
        fator: 1000,
      },
      {
        key: 'total_phosphorus',
        name: 'Total Phosphorus',
        value: 10.66,
        fator: 1000,
      },
      {
        key: 'zinc_compounds',
        name: 'Zinc and compounds',
        value: 0.0175,
        fator: 1000,
      }
    ]
  };
  
  export default dumpData_config;
  