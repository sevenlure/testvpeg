export const DD_MM_YYYY = 'DD/MM/YYYY'
export const DD_MM_YYYY_HH_MM = 'DD/MM/YYYY HH:mm'
export const TIME_ZONE = "Asia/Ho_Chi_Minh"


export default {
  DD_MM_YYYY,
  DD_MM_YYYY_HH_MM,
  TIME_ZONE
}
