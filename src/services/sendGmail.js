import { EMAIL_USER_SYSTEM, EMAIL_ALERT_LOGIN, EMAIL_TOKEN_SYSTEM } from 'config'


// console.log('EMAIL_TOKEN_SYSTEM',EMAIL_TOKEN_SYSTEM)
function getContent(email){
  return `<div>
  <div
  align="center"
  class="m_6166498997858349759mdv2rw"
  style="border-style:solid;border-width:thin;border-color:#dadce0;border-radius:8px;padding:40px 20px"
>
  <h2>VPEG</h2>
  <div
    style="font-family:'Google Sans',Roboto,RobotoDraft,Helvetica,Arial,sans-serif;border-bottom:thin solid #dadce0;color:rgba(0,0,0,0.87);line-height:32px;padding-bottom:24px;text-align:center;word-break:break-word"
  >
    <div style="font-size:24px">
      Cảnh báo có sự đăng nhập vào hệ thống của&nbsp;bạn
    </div>
    <table align="center" style="margin-top:8px">
      <tbody>
        <tr style="line-height:normal">
          <td align="right" style="padding-right:8px">
            <img
              height="20"
              src="https://ci4.googleusercontent.com/proxy/QpsGaULeBaBhhOTpb-uwGsICda8b1ae95rM7JtYlDtcjbrJ_fDlrGcQ9nUwocVilT_dWdlntnRieTr4GY_IFycf2zxXXuPXiHCdY7G5yRw7uJHHhalp2NYvY=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/anonymous_profile_photo.png"
              style="width:20px;height:20px;vertical-align:sub;border-radius:50%"
              width="20"
              class="CToWUd"
            />
          </td>
          <td>
            <a
              style="font-family:'Google Sans',Roboto,RobotoDraft,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.87);font-size:14px;line-height:20px"
              >${email}</a
            >
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <div
    style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:14px;color:rgba(0,0,0,0.87);line-height:20px;padding-top:20px;text-align:center"
  >
    
  </div>
</div>
</div>
`
}
// Tài khoản VPEG của bạn vừa được đăng nhập. Bạn
//     nhận được email này vì chúng tôi muốn đảm bảo rằng đó chính là bạn.

var send = require('gmail-send')({
  //var send = require('../index.js')({
    user: EMAIL_USER_SYSTEM, //'VPEG.SYSTEM@gmail.com',
    // user: credentials.user,                  // Your GMail account used to send emails
    pass: EMAIL_TOKEN_SYSTEM,//'sekdrhptwzgazjwb',
    // pass: credentials.pass,                  // Application-specific password
    to:   EMAIL_ALERT_LOGIN, //'phat.duong@vietan-software.com',     
    // to:   credentials.user,                  // Send to yourself
                                             // you also may set array of recipients:
                                             // [ 'user1@gmail.com', 'user2@gmail.com' ]
    // from:    credentials.user,            // from: by default equals to user
    // replyTo: credentials.user,            // replyTo: by default undefined
    // bcc: 'some-user@mail.com',            // almost any option of `nodemailer` will be passed to it
    subject: 'VPEG - Cảnh báo đăng nhập',
    // text:    'VPEG SYSTEM',         // Plain text
    // html:    '<b>html text</b>'            // HTML
  });

  export default {
    send,
    getContent
  }
  