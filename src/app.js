import bodyParser from 'body-parser'
import express from 'express'
import mongoose, { connection } from 'mongoose'
import _ from 'lodash'
import moment from 'moment'
import config from './config'
import authMiddleware from './middlewares/authMiddleware'
import facilityRoleMiddleware from './middlewares/facilityRoleMiddleware'
import authRoute from './routes/authRoute'
import facilityRoute from './routes/facilityRoute'
import lookuplistitemRoute from './routes/lookuplistitemRoute'
import lookuplistRoute from './routes/lookuplistRoute'
import provinceRoute from './routes/provinceRoute'
import facilityattachmentRoute from './routes/facilityattachmentRoute'
import photoRoute from './routes/photoRoute'
import facilityeiaRoute from './routes/facilityeiaRoute'
import eiaattachmentRoute from './routes/eiaattachmentRoute'
import facilitypermitRoute from './routes/facilitypermitRoute'
import permitattachmentRoute from './routes/permitattachmentRoute'
import facilityemissionRoute from './routes/facilityemissionRoute'

import facilityInspectionRoute from 'routes/facilityInspectionRoute'
import inspectionAttachmentsRoute from 'routes/inspectionAttachmentsRoute'
import facilityEnviProtectFeeRoute from 'routes/facilityEnviProtectFeeRoute'
import facilityEnviMonitoringReportRoute from 'routes/facilityEnviMonitoringReportRoute'
import monitoringRoute from 'routes/monitoringRoute'
import facilityHazarWasteReportRoute from 'routes/facilityHazarWasteReportRoute'
import userRoute from 'routes/userRoute'
import roleRoute from 'routes/roleRoute'
import logRoute from 'routes/logRoute'
import PRTRLoDotRoute from 'routes/PRTRLoDotRoute'
import PRTRNMXLNuocThaiRoute from 'routes/PRTRNMXLNuocThaiRoute'
import PRTRReportRoute from 'routes/PRTRReportRoute'
import PRTRConfig from 'routes/PRTRConfig'
import PRTRFacilityRoute from 'routes/PRTRFacilityRoute'


import { DATE_FORMAT, BLACKLIST_JSON_PATH } from './config'

import testRoute from 'routes/testRoute'
import JsonDB from 'node-json-db'
// const NodeCache = require( "node-cache" );
// const myCache = new NodeCache();
const dbMyJsonDataBase = new JsonDB("myJsonDataBase", true, false);


// var data = db.getData("/test1");
// console.log(data)

// myCache.set( "myKey", 'myValue')
// console.log('cache',myCache.get('myKey'))

// Init app express
const app = express()

global._ = _
global.moment = moment
global.DATE_FORMAT = DATE_FORMAT
global.BLACKLIST_JSON_PATH = BLACKLIST_JSON_PATH
global.dbMyJsonDataBase = dbMyJsonDataBase
// global.connection = mongoose.connection
//Connect mongodb
mongoose
  .connect(
    config.MONGODB_OPTIONS.database,
    config.MONGODB_OPTIONS.opt
  )
  .then(res => {
    console.log('Connected to Database Successfully.')
  })
  .catch(() => {
    console.log('Conntection to database failed.')
  })

app.use(function(req, res, next) {
  if (app.get('env') === 'development') {
    res.setHeader('Access-Control-Allow-Origin', '*')
  } else {
    res.setHeader('Access-Control-Allow-Origin', '*')
  }

  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE'
  )
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-Requested-With,content-type, Cache-Control, Authorization'
  )
  res.setHeader('Access-Control-Allow-Credentials', true)
  res.shouldKeepAlive = false
  next()
})
// dbMyJsonDataBase.push(BLACKLIST_JSON_PATH,{tonanh:'tonanh2'},false);
// dbMyJsonDataBase.push(BLACKLIST_JSON_PATH,{maithao:'maithaoToken'},false);

// var data = dbMyJsonDataBase.getData(BLACKLIST_JSON_PATH);
// console.log('sadasdasdasdas',data)


// Setup bodyparser
app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }))
app.use(bodyParser.json({limit: '50mb'}))

// Routes
app.get('/', (req, res) => {
  res.json({ message: 'Hello world' })
})

app.use(express.static('public'))



app.use('/auth', authRoute)

//Qui rem lại tạm thời để chạy ưng dụng
app.use(authMiddleware)
app.use(facilityRoleMiddleware)

// app.use((req, res, next) => {
//   let json = res.json.bind(res);
//   res.json = (data) => {
//     let newData = {...data, key:'ssssssssss'}
//     return json(newData);
//   };
//   next();
// });


app.use('/facility', facilityRoute)
app.use('/lookuplistitem', lookuplistitemRoute)
app.use('/lookuplist', lookuplistRoute)
app.use('/province', provinceRoute)
app.use('/facilityattachment', facilityattachmentRoute)
app.use('/file', photoRoute)
app.use('/facilityeia', facilityeiaRoute)
app.use('/eiaattachment', eiaattachmentRoute)
app.use('/facilitypermit', facilitypermitRoute)
app.use('/permitattachment', permitattachmentRoute)
app.use('/facilityemission', facilityemissionRoute)
app.use('/facilityInspection', facilityInspectionRoute)
app.use('/inspectionAttachment', inspectionAttachmentsRoute)
app.use('/facilityEnviProtectFee', facilityEnviProtectFeeRoute)
app.use('/facilityEnviMonitoringReport', facilityEnviMonitoringReportRoute)
app.use('/monitoring', monitoringRoute)
app.use('/facilityHazarWasteReport', facilityHazarWasteReportRoute)
app.use('/user', userRoute)
app.use('/role', roleRoute)
app.use('/log', logRoute)


// NOTE  api của PRTR 
app.use('/PRTR-incinerator', PRTRLoDotRoute)
app.use('/PRTR-water-treatment-factory', PRTRNMXLNuocThaiRoute)
app.use('/PRTR-report', PRTRReportRoute)
app.use('/PRTR-config', PRTRConfig)
app.use('/PRTR-facility', PRTRFacilityRoute)


app.use('/testRoute', testRoute)

var server = app.listen(config.PORT, () => {
  console.log(`start server on ${config.PORT}`)
  server.keepAliveTimeout = 0
})
