import express from 'express'
import path from 'path'
import createMulter from '../utils/multer'
import { PUBLIC_URL, FOLDER_UPLOAD } from '../config'
import { resizeImageTo1024 } from '../utils'

let router = express.Router()

function getInfo(customPath = '/', filename) {
  return {
    url: PUBLIC_URL + '/' + path.join('FileDatabase', customPath, filename),
    path: path.join(FOLDER_UPLOAD, customPath, filename)
  }
}

router.post('/upload', createMulter('/').single('file'), (req, res) => {
  const fileInfo = getInfo('/', req.file.filename)
  if (_.startsWith('image', req.file.mimetype)) {
    resizeImageTo1024(fileInfo.path, () => {
      res.json({
        url: fileInfo.url,
        file: req.file
      })
    })
  } else {
    res.json({
      url: fileInfo.url,
      file: req.file
    })
  }
})

/**
 * Upload with directory middleware
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function middlewareDirectory(req, res, next) {
  const directory = req.params.directory
  createMulter(directory).single('file')(req, res, next)
}

router.post(
  '/uploadWithDirectory/:directory',
  middlewareDirectory,
  (req, res) => {
    const fileInfo = getInfo(req.params.directory, req.file.filename)
    resizeImageTo1024(fileInfo.path, () => {
      res.json({
        url: fileInfo.url,
        file: req.file
      })
    })
  }
)

export default router
