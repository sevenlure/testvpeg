import facilityDao from 'dao/facilityDao'
import { FacilitySimpleModel } from 'models/Facility'
import BaseRoute from './baseRoute'
import { resError } from 'utils/res'
import express from 'express'
import excel from 'node-excel-export'
import specification from 'exportConfig/facility'
import {connection} from 'mongoose'
import Log from 'models/Log'
import {TYPE_EVENT} from 'models/Log'
// import fs from 'fs'

const router = express.Router()
export class FacilityRoute extends BaseRoute {
  constructor() {
    router.post('/checkFacilityCodeExist', async (req, res) => {
      try {
        console.log('aaa')
        let item = await this.checkFacilityCode(
          req.body.FacilityCode,
          req.body.oldCode
        )
        let result = { exist: item ? true : false, data: item }
        res.json({ success: true, data: result })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.get('/getAll', async (req, res) => {
      try {
        let query = _.pick(req.query, this.queryProps)
        if (this.fieldNameSearch && req.query.search) {
          query = {
            ...query,
            $or: [
              {
                [this.fieldNameSearch]: { $regex: `${req.query.search}` }
              },
              {
                FacilityCode: { $regex: `${req.query.search}` }
              }
            ]
          }
        }
        
        const totalItem = await facilityDao.getTotalCount(query)

        let field = []
        if (req.query.field && req.query.field.length > 0)
          field = req.query.field

        const itemList = await FacilitySimpleModel.find(
          {},
          {
            FacilityCode: 1,
            FacilityNameFull: 1,
            Latitude: 1,
            Longitude: 1,
            industrialArea: 1,
            province: 1,
            sector: 1,
            district: 1,
            authority: 1,
            AddedOn: 1
          }
        )

        res.json({ success: true, data: itemList, totalItem })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.get('/export', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})
        let itemList = await FacilitySimpleModel.find({}, {})
          .populate('industrialArea')
          .populate('district')
          .populate('sector')
          .populate('authority')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // fs.writeFile('public/exports/users.xlsx', report, function (err) {
        //   if (err) return console.log(err);
        //   else return res.render('receive', {link: `${domain}/exports/users.xlsx`});
        // });

        // You can then return this straight
        // res.attachment('Cơ sở Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Cơ sở Data.xlsx' })
      } catch (e) {
        res.json({ error: true, message: 'fail' })
        // resError(res, e.message)
        throw e
      }
    })

    router.post('/exportWithQuery', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})
        let query = {}
        if(req.body._id){
          query = {
            _id: {
              $in: req.body._id
            }
          }
        }
        console.log('testQuery',req.body)
        let itemList = await FacilitySimpleModel.find(query, {})
          .populate('industrialArea')
          .populate('district')
          .populate('sector')
          .populate('authority')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // fs.writeFile('public/exports/users.xlsx', report, function (err) {
        //   if (err) return console.log(err);
        //   else return res.render('receive', {link: `${domain}/exports/users.xlsx`});
        // });

        // You can then return this straight
        // res.attachment('Cơ sở Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Cơ sở Data.xlsx' })
      } catch (e) {
        res.json({ error: true, message: 'fail' })
        // resError(res, e.message)
        throw e
      }
    })

    //Loger
    router.post('/', async (req, res, next) => {
      try {
        let item = req.body
        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.CREATE,
          LogEvent: `Thêm mới cơ sở:
          Tên Cơ Sở: ${item.FacilityNameFull}
          Giấy phép: ${item.BusinessLicenseNumber}
          Mã chi nhánh: ${item.Branchcode}
          Mã cơ sở: ${item.FacilityCode}
          Tần suất giám sát: ${item.FrequencyOfMonitoring}
          ...
          `
        })
        next()
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.put('/:_id', async (req, res, next) => {
      try {
        let item = req.body
        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.UPDATE,
          LogEvent: `Chỉnh sửa cơ sở:
          _id: ${req.params._id}
          Tên Cơ Sở: ${item.FacilityNameFull}
          Giấy phép: ${item.BusinessLicenseNumber}
          Mã chi nhánh: ${item.Branchcode}
          Mã cơ sở: ${item.FacilityCode}
          Tần suất giám sát: ${item.FrequencyOfMonitoring}
          ...
          `
        })
        next()
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.delete('/:_id', async (req, res, next) => {
      try {
        let item = await facilityDao.findOne({ _id: req.params._id})
        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.DELETE,
          LogEvent: `Xoá cơ sở:
          _id: ${req.params._id}
          Tên Cơ Sở: ${item.FacilityNameFull}
          Giấy phép: ${item.BusinessLicenseNumber}
          Mã chi nhánh: ${item.Branchcode}
          Mã cơ sở: ${item.FacilityCode}
          Tần suất giám sát: ${item.FrequencyOfMonitoring}
          ...
          `
        })
        next()
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })
     //END Loger

    super(facilityDao, router)
    this.queryProps = [
      '_id',
      'FacilityID',
      'FacilityNameFull',
      'district',
      'sector',
      'industrialArea',
      'authority'
    ]
    this.updateProps = [
      'FacilityID',
      'FacilityNameFull',
      'industrialArea',
      'authority',
      'sector',
      'Telephone',
      'Fax',
      'FacilityCode',
      'Aream2',
      'Latitude',
      'Longitude',
      'Address1',
      'district',
      'Commune',
      'PContactFirst',
      'PContactTel',
      'PContactEmail',
      'SContactFirst',
      'SContactTel',
      'SContactEmail',
      'ReceviedSource_1',
      'ReceviedSource_2',
      'ReceviedSource_3',
      'ReceviedSource_4',
      'InvestmentLicenseNumber',
      'InvestmentLicenseIssueDateFirst',
      'InvestmentLicenseIssueDateLast',
      'BusinessLicenseIssueDateFirst',
      'BusinessLicenseIssueDateLast',
      'BusinessLicenseNumber',
      'YearsOfOperation',
      'Employees',
      'Capacity',
      'ProductDesc',
      'Fuel',
      'MainMaterials',
      'MaterialConsumption',
      'WorkingDaysMonth',
      'WorkingDaysYear',
      'WaterUseDay',
      'WaterUseMonth',
      'waterSource',
      'ownership',
      'FrequencyOfMonitoring',
    ]
    this.fieldNameSearch = 'FacilityNameFull'
  }

  async checkFacilityCode(FacilityCode, oldCode) {
    return await facilityDao.findOne({
      FacilityCode,
      $and: [{ FacilityCode: { $ne: oldCode } }]
    })
  }
}

export default new FacilityRoute().router
