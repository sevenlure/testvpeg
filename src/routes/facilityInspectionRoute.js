import facilityInspectionDao from 'dao/facilityInspectionDao'
import BaseRoute from 'routes/baseRoute'
import { resError } from 'utils/res'
import {
  FacilityInspectionSimpleModel,
  FacilityInspectionBCTKModel
} from 'models/FacilityInspection'
import express from 'express'
import excel from 'node-excel-export'
import specification from 'exportConfig/facilityInspection'
import Log, {TYPE_EVENT} from 'models/Log'
import Facility from 'models/Facility'

const router = express.Router()
export class facilityInspectionRoute extends BaseRoute {
  constructor() {
    router.get('/export', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})

        let itemList = await FacilityInspectionSimpleModel.find({}, {})
          .populate('inspectionStatus')
          .populate('complianceType')
          .populate('purposeObj')
          .populate('facility')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // You can then return this straight
        // res.attachment('Thanh tra Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Thanh tra Data.xlsx' })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.post('/exportWithQuery', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})
        let query = {}
        if(req.body._id){
          query = {
            _id: {
              $in: req.body._id
            }
          }
        }

        let itemList = await FacilityInspectionSimpleModel.find(query, {})
          .populate('inspectionStatus')
          .populate('complianceType')
          .populate('purposeObj')
          .populate('facility')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // You can then return this straight
        // res.attachment('Thanh tra Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Thanh tra Data.xlsx' })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.get('/getAll', async (req, res) => {
      try {
        let query = _.pick(req.query, this.queryProps)
        if (this.fieldNameSearch && req.query.search) {
          query = {
            ...query,
            $or: [
              {
                [this.fieldNameSearch]: { $regex: `${req.query.search}` }
              },
              {
                FacilityCode: { $regex: `${req.query.search}` }
              }
            ]
          }
        }
        const totalItem = await facilityInspectionDao.getTotalCount(query)

        let field = []
        if (req.query.field && req.query.field.length > 0)
          field = req.query.field
       
        const itemList = await FacilityInspectionBCTKModel.find(
          {},
          {
            facility: 1,
            InspectionDate: 1,
            inspectionStatus: 1,
            purposeObj: 1,
            complianceType: 1,
            ComplianceFine: 1,
            AddedOn: 1
          }
        )
        .populate('facility')
        .lean()
        const to = moment()

        res.json({ success: true, data: itemList, totalItem })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

        //Loger
        router.post('/', async (req, res, next) => {
          try {
            let item = req.body
            let facility = await Facility.findOne({_id: item.facility })
            let TenCoSo = facility? facility.FacilityNameFull : ""
            Log.create({
              user: req.user ? req.user._id : undefined,
              UserName: req.user ? req.user.UserName : undefined,
              TypeEvent: TYPE_EVENT.CREATE,
              LogEvent: `Thêm mới Thanh tra:
              Cơ sở: ${TenCoSo}
              Ngày Thanh tra: ${moment(item.InspectionDate).format(DATE_FORMAT)}
              ...
              `
            })
            next()
          } catch (e) {
            resError(res, e.message)
            throw e
          }
        })
    
        router.put('/:_id', async (req, res, next) => {
          try {
            let item = req.body
            let facility = await Facility.findOne({_id: item.facility })
            let TenCoSo = facility? facility.FacilityNameFull : ""
            Log.create({
              user: req.user ? req.user._id : undefined,
              UserName: req.user ? req.user.UserName : undefined,
              TypeEvent: TYPE_EVENT.UPDATE,
              LogEvent: `Chỉnh sửa Thanh tra:
              _id: ${req.params._id}
              Cơ sở: ${TenCoSo}
              Ngày Thanh tra: ${moment(item.InspectionDate).format(DATE_FORMAT)}
              ...
              `
            })
            next()
          } catch (e) {
            resError(res, e.message)
            throw e
          }
        })
    
        router.delete('/:_id', async (req, res, next) => {
          try {
            let item = await facilityInspectionDao.findOne({ _id: req.params._id})
            let facility = await Facility.findOne({_id: item.facility })
            let TenCoSo = facility? facility.FacilityNameFull : ""
            Log.create({
              user: req.user ? req.user._id : undefined,
              UserName: req.user ? req.user.UserName : undefined,
              TypeEvent: TYPE_EVENT.DELETE,
              LogEvent: `Xoá thanh tra:
              _id: ${req.params._id}
              Cơ Sở: ${TenCoSo}
              Ngày Thanh tra: ${moment(item.InspectionDate).format(DATE_FORMAT)}
              ...
              `
            })
            next()
          } catch (e) {
            resError(res, e.message)
            throw e
          }
        })
         //END Loger

    super(facilityInspectionDao, router)
    this.queryProps = [
      'FacilityID',
      'FacilityNameFull',
      'district',
      'sector',
      'industrialArea',
      'authority',
      'sector',
      'inspectionStatus'
    ]
    this.updateProps = [
      'InspectionDate',
      'Notes',
      'complianceType',
      'facility',
      'inspectionStatus',
      'purposeObj',
      'ComplianceFine',
      'complianceFile'
    ]
    this.fieldNameSearch = 'FacilityNameFull'

    this.router.post('/checkFacilityCodeExist', async (req, res) => {
      try {
        let item = await this.checkFacilityCode(
          req.body.FacilityCode,
          req.body.oldCode
        )
        let result = { exist: item ? true : false, data: item }
        res.json({ success: true, data: result })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })
  }

  async checkFacilityCode(FacilityCode, oldCode) {
    return await this.objDao.findOne({
      FacilityCode,
      $and: [{ FacilityCode: { $ne: oldCode } }]
    })
  }

  handleQueryGet(query) {
    let res = super.handleQueryGet(query)
    if (query.year)
      res = {
        ...res,
        $expr: { $eq: [{ $year: '$InspectionDate' }, Number(query.year)] }
      }
    return res
  }
}

export default new facilityInspectionRoute().router
