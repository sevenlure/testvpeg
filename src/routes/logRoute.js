import logDao from 'dao/logDao'
import BaseRoute from './baseRoute'

export class LogRoute extends BaseRoute {
  constructor() {
    super(logDao)
    this.queryProps = ['user', 'TypeEvent']
    this.updateProps = []
  }
  handleQueryGet(query) {
    let res = super.handleQueryGet(query)
    if (query.fromDate && query.toDate)
      res.AddedOn = {
        $gte: query.fromDate,
        $lt: query.toDate
      }
    return res
  }
}

export default new LogRoute().router
