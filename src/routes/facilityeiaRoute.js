import facilityeiaDao from 'dao/facilityeiaDao'
import BaseRoute from './baseRoute'
import express from 'express'
import { FacilityeiaBCTKModel } from 'models/Facilityeia'
import { resError } from 'utils/res'
import excel from 'node-excel-export'
import specification from 'exportConfig/facilityeia'


const router = express.Router()
export class FacilityeiaRoute extends BaseRoute {
  constructor() {
    router.get('/getAll', async (req, res) => {
      try {
        let query = _.pick(req.query, this.queryProps)
        if (this.fieldNameSearch && req.query.search) {
          query = {
            ...query,
            $or: [
              {
                [this.fieldNameSearch]: { $regex: `${req.query.search}` }
              },
              {
                FacilityCode: { $regex: `${req.query.search}` }
              }
            ]
          }
        }
        const totalItem = await facilityeiaDao.getTotalCount(query)

        let field = []
        if (req.query.field && req.query.field.length > 0)
          field = req.query.field

        const itemList = await FacilityeiaBCTKModel.find(
          {},
          {
            facility: 1,
            IsCompleteDTM: 1,
            IsEnvironmentalIncident: 1,
            DateIssued: 1,
            AddedOn: 1,
            Title: 1
          }
        )
          .populate('facility')
          .lean()
        const to = moment()

        res.json({ success: true, data: itemList, totalItem })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.get('/export', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})

        let itemList = await FacilityeiaBCTKModel.find({}, {})
          .populate('emissionType')
          .populate('facility')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // You can then return this straight
        // res.attachment('Quản lý ô nhiễm Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Hồ sơ môi truờng Data.xlsx'})
      } catch (e) {
        res.json({
          error: true,
          message: JSON.stringify(e)
        })
        throw e
      }
    })

    router.post('/exportWithQuery', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})
        let query = {}
        if(req.body._id){
          query = {
            _id: {
              $in: req.body._id
            }
          }
        }

        let itemList = await FacilityeiaBCTKModel.find(query, {})
          .populate('emissionType')
          .populate('facility')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // You can then return this straight
        // res.attachment('Quản lý ô nhiễm Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Hồ sơ môi truờng Data.xlsx'})
      } catch (e) {
        res.json({
          error: true,
          message: JSON.stringify(e)
        })
        throw e
      }
    })

    super(facilityeiaDao, router)
    this.queryProps = ['facility', 'sector']
    this.updateProps = [
      'Title',
      'DateIssued',
      'EIAURL',
      'Remarks',
      'DecisionNumber',
      'IssuingAgency',
      'IsCompleteDTM',
      'IsEnvironmentalIncident'
    ]
  }
}

export default new FacilityeiaRoute().router
