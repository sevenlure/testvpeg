import express from 'express'

import authMiddleware from 'middlewares/authMiddleware'
import Errors from 'constants/errors'
import authDao from 'dao/authDao'
import { resError } from 'utils/res'
import { JWT_SECRET } from 'config'
import { buildCleaner } from 'lodash-clean'
import Role from 'models/Role'
import Log from 'models/Log'
import {TYPE_EVENT} from 'models/Log'
import Queue from 'grouped-queue'
import gmail from 'services/sendGmail'

var queue = new Queue();

function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const options = {
  isBoolean: val => {
    return val || undefined
  }
}
const clean = buildCleaner(options)
var router = express.Router()

function flatObject(input) {
  function flat(res, key, val, pre = '') {
    const prefix = [pre, key].filter(v => v).join('.')
    return typeof val === 'object'
      ? Object.keys(val).reduce(
          (prev, curr) => flat(prev, curr, val[curr], prefix),
          res
        )
      : Object.assign(res, { [prefix]: val })
  }

  return Object.keys(input).reduce(
    (prev, curr) => flat(prev, curr, input[curr]),
    {}
  )
}

/**
 * Register
 * @param body: UserName, Password
 */
router.post('/register', async (req, res) => {
  const userIsExists = await authDao.checkExistsByUserName(req.body.UserName)
  if (userIsExists) {
    resError(res, Errors.USER_REGISTER_EXISTS)
  }
  let userData = {
    UserName: req.body.UserName,
    Password: req.body.Password
  }
  const userCreate = await authDao.register(userData)
  res.json({ success: true, data: userCreate })
})

/**
 * Login
 * @param body: UserName, Password
 */
router.post('/login', async (req, res) => {
  let user = await authDao.login({
    UserName: req.body.UserName,
    Password: req.body.Password
  })
  if (!user) {
    resError(res, Errors.USER_PASSWORD_INCORRECT)
  } else {
    let roleDefault = {}
    if (!user.role) {
      roleDefault =await Role.findOne({
        Key: 'GUEST'
      })
      user.role = roleDefault
    }

    let permissionRole = clean(
      _.result(user, 'role.permissionRole', {})
    )

    Log.create({
      user: user._id,
      UserName: user.UserName,
      TypeEvent: TYPE_EVENT.LOGIN
    })

    queue.add(async (cb)=>{
      gmail.send({
        // to: user.Email,
        html: gmail.getContent(user.UserName)
        // to: item,
        // html: gmail.getContent(item)
      })
      await delay(10000)
      cb()
    })

    res.json({
      success: true,
      token: await authDao.createToken(user),
      // data: user
      data: {
        ...user.toObject(),
        permissionRole: permissionRole? _.keys(flatObject(permissionRole)) : []
      }
    })
  }
})

/**
 * Protected route by **authMiddlware**, require req must have header Authoraization
 */
router.get('/me', authMiddleware, async (req, res) => {
  res.json({ data: req.user })
})

export default router
