import inspectionAttachmentsDao from 'dao/inspectionAttachmentsDao'
import BaseRoute from 'routes/baseRoute'

export class inspectionAttachmentsRoute extends BaseRoute {
  constructor() {
    super(inspectionAttachmentsDao)
    this.queryProps = ['inspection']
    this.updateProps = [

    ]
    //this.fieldNameSearch = 'FacilityNameFull'
  }
}

export default new inspectionAttachmentsRoute().router
