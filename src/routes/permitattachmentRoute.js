import permitattachmentDao from 'dao/permitattachmentDao'
import BaseRoute from './baseRoute'

export class PermitattachmentRoute extends BaseRoute {
  constructor() {
    super(permitattachmentDao)
    this.queryProps = ['facilitypermit']
    this.updateProps = [
   
    ]
    //this.fieldNameSearch = 'FacilityNameFull'
  }
}

export default new PermitattachmentRoute().router
