import PRTRFacilityDao from 'dao/PRTRFacilityDao'
import BaseRoute from './baseRoute'
import { resError } from 'utils/res'
import express from 'express'
import {
  map as _map,
  pick as _pick,
  get as _get,
  includes as _includes,
  toUpper as _toUpper,
  compact as _compact
} from 'lodash'
import mongoose from 'mongoose'

const router = express.Router()

export class PRTRFacilityRoute extends BaseRoute {
  constructor() {
    const dataPick = ['facility', 'ngayHoatDong', 'listLoDot', 'sector']
    router.get('/by-facility/:_id/:MM/:YYYY', async (req, res) => {
      try {
        let query = {}
        // console.log(req.params._id)
        // console.log(req.query)
        query.facility = req.params._id
        query.YYYY = req.params.YYYY
        query.MM = req.params.MM
        // console.log(query)
        const item = await this.objDao.findOne(query)
        res.json({ success: true, data: item })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.get('/map', async (req, res) => {
      try {
        const hoaChat = req.query.hoaChat
        if (!hoaChat) {
          res.json({
            success: true,
            data: []
          })
        }
        let query = {
          sector: mongoose.Types.ObjectId(req.query.sector)
        }
        if (query && req.query.search) {
          query = {
            ...query,
            FacilityNameFull: { $regex: `${req.query.search}`, $options: 'i' }
          }
        }
        // console.log(query, 'query 1')
        let itemList = await this.objDao.getListCoSoPRTR(query, hoaChat)

        res.json({
          success: true,
          data: itemList,
          count: itemList ? itemList.length : 0
        })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    super(PRTRFacilityDao, router)
    this.queryProps = dataPick
    this.updateProps = [
      'facility',
      'MM',
      'YYYY',
      'ngayHoatDong',
      'listLoDot',
      'donViPhanTich',
      'ngayLayMau',
      'ngayTraKetQua'
    ]
  }
}

export default new PRTRFacilityRoute().router
