import lookuplistitemDao from 'dao/lookuplistitemDao'
import BaseRoute from './baseRoute'
import { resError } from 'utils/res'
import express from 'express'
import { connection, Types } from 'mongoose'
import { map as _map, pick as  _pick } from 'lodash'
const router = express.Router()

export class LookuplistitemRoute extends BaseRoute {
  constructor() {

    
    router.get('/getAll', async (req, res) => {
      try {
        let query = _.pick(req.query, ['Name', 'ProvinceID', 'lookuplist'])
        const itemList = await this.objDao.getList(query, {})

        res.json({ success: true, data: itemList })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.delete('/deleteWithCheck/:_id', async (req, res) => {
      try {
        let countReference = await connection
          .collection(req.body.collection)
          .countDocuments({
            [req.body.path]: Types.ObjectId(req.params._id) //req.params._id
          })
        if (countReference > 0) {
          res.json({ error: true, countReference })
          return
        } else
          await this.objDao.delete({
            _id: req.params._id
          })
        res.json({ success: true })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    super(lookuplistitemDao, router)
    this.queryProps = ['Name', 'ProvinceID', '_id', 'lookuplist','PRTRKey']
    this.updateProps = ['Key', 'Name', 'DisplaySequence', 'ExtraInfo','Options']

    this.router.get('/lookuplist/:LookupListID', async (req, res) => {
      try {
        // console.log('req.params', req.params)
        let query = _.pick(req.query, this.queryProps)
        let item = await lookuplistitemDao.getList(query, {})
        item = item.filter(item => {
          return item.lookuplist.LookupListID == req.params.LookupListID
        })
        res.json({ success: true, data: item })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })
    

    router.put('/updateChildren/:_id', async (req, res) => {
      try {
        const query = { _id: req.params._id, 'children.key': req.body.key }
        let itemExist = await this.objDao.findOne(query)
        if (!itemExist) resError(res, 'ITEMS_NOT_EXISTS')
        const itemData = _.pick(req.body, this.updateProps)
        let record = await this.objDao.update(query, {
          'children.$.name': req.body.name,
          UpdatedBy: req.user ? req.user.UserName : undefined
        })
        res.json({ success: true, data: record })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    
  }
}

export default new LookuplistitemRoute().router
