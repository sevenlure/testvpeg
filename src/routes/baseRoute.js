import express from 'express'
import { resError } from 'utils/res'
import { getPagination } from 'utils/pagination'

export default class BaseRoute {
  constructor(objDao, router, notInherit = {}) {
    this.objDao = objDao
    this.queryProps = []
    this.queryExists = []
    this.updateProps = []
    this.fieldNameSearch = ''
    this.router = router ? router : express.Router()

    //add router
    this.router.post('/', async (req, res) => {
      try {
        let item = req.body
        let record = await this.objDao.create({
          ...item,
          AddedBy: req.user ? req.user.UserName : undefined
        })
        res.json({ success: true, data: record })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    this.router.get('/', async (req, res) => {
      try {
        let query = this.handleQueryGet(req.query)
        // if (this.fieldNameSearch && req.query.search)
        //   query[this.fieldNameSearch] = { $regex: `${req.query.search}` }
        if (this.fieldNameSearch && req.query.search) {
          query = {
            ...query,
            // $text: { $search: `"${req.query.search}"` }
            $or: [
              {
                [this.fieldNameSearch]: {
                  $regex: `${req.query.search}`,
                  $options: 'i'
                }
              },
              {
                FacilityCode: { $regex: `${req.query.search}` }
              }
            ]
          }
        }
        // console.log("--QUERY--")

        const totalItem = await this.objDao.getTotalCount(query)
        const pagination = getPagination(req, totalItem)
        let field = []
        if (req.query.field && req.query.field.length > 0)
          field = req.query.field
        const itemList = await this.objDao.getList(query, pagination, field)

        res.json({ success: true, data: itemList, pagination })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    this.router.get('/:_id', async (req, res) => {
      try {
        const query = {}
        query._id = req.params._id
        if (!query._id || query._id === 'undefined') {
          res.json({ error: true, message: '' })
          return 
        }
        const item = await this.objDao.findOne(query)
        res.json({ success: true, data: item })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    if (!notInherit.isEdit)
      this.router.put('/:_id', async (req, res) => {
        try {
          const query = { _id: req.params._id }
          let itemExist = await this.objDao.findOne(query)
          if (!itemExist) resError(res, 'ITEMS_NOT_EXISTS')
          const itemData = _.pick(req.body, this.updateProps)
          let record = await this.objDao.update(query, {
            ...itemData,
            UpdatedBy: req.user ? req.user.UserName : undefined
          })
          res.json({ success: true, data: record })
        } catch (e) {
          resError(res, e.message)
          throw e
        }
      })

    this.router.delete('/:_id', async (req, res) => {
      try {
        let success = await this.objDao.delete({
          _id: req.params._id
        })
        res.json({ success: true })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })
  }

  handleQueryGet(query) {
    let res = _.pick(query, this.queryProps)
    return res
  }
}
