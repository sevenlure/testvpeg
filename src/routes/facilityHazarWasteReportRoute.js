import FacilityHazarWasteReportDao from 'dao/facilityHazarWasteReportDao'
import BaseRoute from './baseRoute'

import FacilityHazarWasteReportModel from 'models/FacilityHazarWasteReport'
import { resError } from 'utils/res'
import express from 'express'
import excel from 'node-excel-export'
import specification from 'exportConfig/facilityHazarWasteReportDao'
import Log, { TYPE_EVENT } from 'models/Log'
import Facility from 'models/Facility'
const router = express.Router()

export class FacilityHazarWasteReportRoute extends BaseRoute {
  constructor() {
    router.get('/export', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})

        let itemList = await FacilityHazarWasteReportModel.find(
          {},
          {}
        ).populate('facility')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // You can then return this straight
        // res.attachment('Phí bảo vệ môi trường Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Chất thải nguy hại Data.xlsx' })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.post('/exportWithQuery', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})
        let query = {}
        if (req.body._id) {
          query = {
            _id: {
              $in: req.body._id
            }
          }
        }

        let itemList = await FacilityHazarWasteReportModel.find(
          query,
          {}
        ).populate('facility')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // You can then return this straight
        // res.attachment('Phí bảo vệ môi trường Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Chất thải nguy hại Data.xlsx' })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.get('/getAll', async (req, res) => {
      try {
        let query = _.pick(req.query, this.queryProps)
        if (this.fieldNameSearch && req.query.search) {
          query = {
            ...query,
            $or: [
              {
                [this.fieldNameSearch]: { $regex: `${req.query.search}` }
              },
              {
                FacilityCode: { $regex: `${req.query.search}` }
              }
            ]
          }
        }
        const totalItem = await FacilityHazarWasteReportDao.getTotalCount(query)

        let field = []
        if (req.query.field && req.query.field.length > 0)
          field = req.query.field

        const itemList = await FacilityHazarWasteReportModel.find(
          {},
          {
            facility: 1,
            AddedOn: 1,
            Year: 1,
            collector: 1,
            Volume: 1,
            PaymentStatus: 1,
            SubmitDate: 1
          }
        )
          .populate('facility')
          .lean()

        const to = moment()

        res.json({ success: true, data: itemList, totalItem })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    //Loger
    router.post('/', async (req, res, next) => {
      try {
        let item = req.body
        console.log('item', item)
        let facility = await Facility.findOne({ _id: item.facility })
        let TenCoSo = facility ? facility.FacilityNameFull : ''
        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.CREATE,
          LogEvent: `Thêm mới Báo cáo chất thải nguy hại:
          Cơ sở: ${TenCoSo}
          Năm: ${item.Year}
          Ngày nộp: ${moment(item.SubmitDate).format(DATE_FORMAT)}
          Tình trạng: ${item.PaymentStatus}
          ...
          `
        })
        next()
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.put('/:_id', async (req, res, next) => {
      try {
        let item = req.body
        let facility = await Facility.findOne({ _id: item.facility })
        let TenCoSo = facility ? facility.FacilityNameFull : ''
        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.UPDATE,
          LogEvent: `Chỉnh sửa Báo cáo chất thải nguy hại:
          _id: ${req.params._id}
          Cơ sở: ${TenCoSo}
          Năm: ${item.Year}
          Ngày nộp: ${moment(item.SubmitDate).format(DATE_FORMAT)}
          Tình trạng: ${item.PaymentStatus}
          ...
          `
        })
        next()
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.delete('/:_id', async (req, res, next) => {
      try {
        let item = await FacilityHazarWasteReportDao.findOne({
          _id: req.params._id
        })
        let facility = await Facility.findOne({ _id: item.facility })
        let TenCoSo = facility ? facility.FacilityNameFull : ''
        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.DELETE,
          LogEvent: `Xoá Báo cáo chất thải nguy hại:
          _id: ${req.params._id}
          Cơ sở: ${TenCoSo}
          Mã số QLCTNH: ${item.MaSoQLCTNH}
          ...
          `
        })
        next()
      } catch (e) {
        res.json({ error: true, message: e.message })
        throw e
      }
    })
    //END Loger

    super(FacilityHazarWasteReportDao, router)
    this.queryProps = [
      'facility',
      'sector',
      'district',
      'industrialArea',
      'authority'
    ]
    this.updateProps = [
      'Year',
      'SubmitDate',
      'PaymentStatus',
      'collector',
      'Volume',
      'ExtraInfo',
      // NOTE  update
      'MaSoQLCTNH',
      'CapLan',
      'NamThucHienBaoCao',
      'HinhThucNop',
      'NgayNopBaoCao',
      'TenCanBoThuLy',
      'NgayCoYKienTraLoi',
      'SoHieuVanBanTraLoi',
      'TongKhoiLuongCTCNTT',
      'TongKhoiLuongCTCNNH',
      'TongKhoiLuongBunNguyHai',
      'DonViThuGom_CTCNTT',
      'DonViThuGom_CTCNNH'
    ]
    this.fieldNameSearch = 'FacilityNameFull'
  }
}

export default new FacilityHazarWasteReportRoute().router
