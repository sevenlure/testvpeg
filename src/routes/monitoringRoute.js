import sequelize from 'utils/connectMSSQL'
import express from 'express'
import { resError } from 'utils/res'

var router = express.Router()

router.get('/solieumoinhat/:LoaiDiem', async (req, res) => {
  try {
    const record = await sequelize.query(
      `select SoLieu_MoiNhat.MaDiem, ThoiGian, DiemDo.LoaiDiem, DiemDo.KinhDo, DiemDo.ViDo
              ,dbo.[GROUP_CONCAT_SLMN](SoLieu_MoiNhat.MaThongSo, GiaTri, ThongSo.DonVi) as Data
      from SoLieu_MoiNhat 
      Inner Join DiemDo 
        ON DiemDo.MaDiem = SoLieu_MoiNhat.MaDiem
      Inner Join ThongSo 
        ON ThongSo.MaThongSo = SoLieu_MoiNhat.MaThongSo
      where DiemDo.LoaiDiem = :LoaiDiem
      group by SoLieu_MoiNhat.MaDiem, ThoiGian, DiemDo.LoaiDiem, DiemDo.KinhDo, DiemDo.ViDo`,
      {
        replacements: { LoaiDiem: req.params.LoaiDiem },
        type: sequelize.QueryTypes.SELECT,
        raw: true
      }
    )
    res.json({ success: true, data: record })
  } catch (e) {
    // resError(res, e.message)
    res.json({ error: true, message: e, typeError: e.name })
    throw e
  }
})

export default router
