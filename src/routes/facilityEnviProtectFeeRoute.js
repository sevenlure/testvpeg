import FacilityEnviProtectFeeDao from 'dao/facilityEnviProtectFeeDao'
import BaseRoute from './baseRoute'

import { FacilityEnviProtectFeeSimpleModel } from 'models/FacilityEnviProtectFee'
import { resError } from 'utils/res'
import express from 'express'
import excel from 'node-excel-export'
import specification from 'exportConfig/facilityEnviProtectFee'
import Log, {TYPE_EVENT} from 'models/Log'
import Facility from 'models/Facility'
const router = express.Router()

export class FacilityEnviProtectFeeRoute extends BaseRoute {
  constructor() {

    router.get('/export', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})

        let itemList = await FacilityEnviProtectFeeSimpleModel.find({}, {})
          .populate('facility')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // You can then return this straight
        // res.attachment('Phí bảo vệ môi trường Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Phí bảo vệ môi trường Data.xlsx'})
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.post('/exportWithQuery', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})
        let query = {}
        if(req.body._id){
          query = {
            _id: {
              $in: req.body._id
            }
          }
        }

        let itemList = await FacilityEnviProtectFeeSimpleModel.find(query, {})
          .populate('facility')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // You can then return this straight
        // res.attachment('Phí bảo vệ môi trường Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Phí bảo vệ môi trường Data.xlsx'})
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.get('/getAll', async (req, res) => {
      try {
        let query = _.pick(req.query, this.queryProps)
        if (this.fieldNameSearch && req.query.search) {
          query = {
            ...query,
            $or: [
              {
                [this.fieldNameSearch]: { $regex: `${req.query.search}` }
              },
              {
                FacilityCode: { $regex: `${req.query.search}` }
              }
            ]
          }
        }
        const totalItem = await FacilityEnviProtectFeeDao.getTotalCount(query)

        let field = []
        if (req.query.field && req.query.field.length > 0)
          field = req.query.field

        const itemList = await FacilityEnviProtectFeeSimpleModel.find(
          {},
          {
            facility: 1,
            AddedOn: 1,
            YearOfCharge: 1,
            QuaterOfCharge: 1,
            PaymentStatus: 1,
            SubmitDate: 1,
            SubmitTime: 1,
            SubjectOfCharge: 1,
            AuthorityBVMT: 1,
            Payment: 1
          }
        )
          .populate('facility')
          .lean()

        const to = moment()

        res.json({ success: true, data: itemList, totalItem })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

         //Loger
         router.post('/', async (req, res, next) => {
          try {
            let item = req.body
            let facility = await Facility.findOne({_id: item.facility })
            let TenCoSo = facility? facility.FacilityNameFull : ""
            Log.create({
              user: req.user ? req.user._id : undefined,
              UserName: req.user ? req.user.UserName : undefined,
              TypeEvent: TYPE_EVENT.CREATE,
              LogEvent: `Thêm mới Phí bảo vệ môi truờng:
              Cơ sở: ${TenCoSo}
              Địa chỉ phát sinh: ${item.IncurredAddress}
              Điện thoại: ${item.Phone}
              Qúi tính phí: ${item.QuaterOfCharge}
              Thuộc đối tuợng thu phí: ${item.SubjectOfCharge}
              Thẩm quyền: ${item.AuthorityBVMT}
              ...
              `
            })
            next()
          } catch (e) {
            resError(res, e.message)
            throw e
          }
        })
    
        router.put('/:_id', async (req, res, next) => {
          try {
            let item = req.body
            let facility = await Facility.findOne({_id: item.facility })
            let TenCoSo = facility? facility.FacilityNameFull : ""
            Log.create({
              user: req.user ? req.user._id : undefined,
              UserName: req.user ? req.user.UserName : undefined,
              TypeEvent: TYPE_EVENT.UPDATE,
              LogEvent: `Chỉnh sửa Phí bảo vệ môi truờng:
              _id: ${req.params._id}
              Cơ sở: ${TenCoSo}
              Địa chỉ phát sinh: ${item.IncurredAddress}
              Điện thoại: ${item.Phone}
              Qúi tính phí: ${item.QuaterOfCharge}
              Thuộc đối tuợng thu phí: ${item.SubjectOfCharge}
              Thẩm quyền: ${item.AuthorityBVMT}
              ...
              `
            })
            next()
          } catch (e) {
            resError(res, e.message)
            throw e
          }
        })
    
        router.delete('/:_id', async (req, res, next) => {
          try {
            let item = await FacilityEnviProtectFeeDao.findOne({ _id: req.params._id})
            let facility = await Facility.findOne({_id: item.facility })
            let TenCoSo = facility? facility.FacilityNameFull : ""
            Log.create({
              user: req.user ? req.user._id : undefined,
              UserName: req.user ? req.user.UserName : undefined,
              TypeEvent: TYPE_EVENT.DELETE,
              LogEvent: `Xoá Phí bảo vệ môi truờng:
              _id: ${req.params._id}
              Cơ sở: ${TenCoSo}
              Địa chỉ phát sinh: ${item.IncurredAddress}
              Điện thoại: ${item.Phone}
              Qúi tính phí: ${item.QuaterOfCharge}
              Thuộc đối tuợng thu phí: ${item.SubjectOfCharge}
              Thẩm quyền: ${item.AuthorityBVMT}
              ...
              `
            })
            next()
          } catch (e) {
            res.json({error: true, message: e.message})
            throw e
          }
        })
         //END Loger

    super(FacilityEnviProtectFeeDao, router)
    this.queryProps = ['facility', 'sector','district','industrialArea', 'authority']
    this.updateProps = [
      'IncurredAddress',
      'Phone',
      'Email',
      'Fax',
      'YearOfCharge',
      'QuaterOfCharge',
      'SubjectOfCharge',
      'AuthorityBVMT',
      'Indebtedness',
      'PaymentStatus',
      'Payment',
      
      'AverageWaterDemand',
      'AverageWasteWater',
      'AverageWasteWaterEnviProfile',
      'AverageWasteWaterActual',
      'TotalWasteWaterOfQuarter',
      'WastewaterContent',

      // NOTE  Update
      'TenCanBoThuLy',
      'QuyMo',
      'LuuLuongNuocThai',
      'Quy',
      'Nam',
      'NgayNopToKhai',
      'NgayCoVanBanThongBao',
      'SoHieuVanBanTraLoi',
      'ThongBaoLan',
      'LoaiThongBao',
      'TongTienTheoThongBaoNopPhi',
      'TinhTrangNop',
      'NamThongBaoNopPhi'
    ]
    this.fieldNameSearch = 'FacilityNameFull'

  }
}

export default new FacilityEnviProtectFeeRoute().router
