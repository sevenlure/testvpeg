import facilityemissionDao from 'dao/facilityemissionDao'
import BaseRoute from './baseRoute'
import { FacilityemissionSimpleModel } from 'models/Facilityemission'
import { resError } from 'utils/res'
import express from 'express'
import excel from 'node-excel-export'
import specification from 'exportConfig/facilityemission'
import Log, {TYPE_EVENT} from 'models/Log'
import Facility from 'models/Facility'
const router = express.Router()
export class FacilityemissionRoute extends BaseRoute {
  constructor() {
    router.get('/export', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})

        let itemList = await FacilityemissionSimpleModel.find({}, {})
          .populate('emissionType')
          .populate('facility')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // You can then return this straight
        // res.attachment('Quản lý ô nhiễm Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Quản lý ô nhiễm Data.xlsx'})
      } catch (e) {
        res.json({
          error: true,
          message: JSON.stringify(e)
        })
        throw e
      }
    })

    router.post('/exportWithQuery', async (req, res) => {
      try {
        // const totalItem = await facilityDao.getTotalCount({})
        let query = {}
        if(req.body._id){
          query = {
            _id: {
              $in: req.body._id
            }
          }
        }

        let itemList = await FacilityemissionSimpleModel.find(query, {})
          .populate('emissionType')
          .populate('facility')

        let report = excel.buildExport([
          // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
          {
            name: 'Data', // <- Specify sheet name (optional)
            // heading: heading, // <- Raw heading array (optional)
            // merges: merges, // <- Merge cell ranges
            specification: specification, // <- Report specification
            data: itemList.map((item, index) => {
              return _.extend(item, { index: index + 1 })
            })
          }
        ])

        // You can then return this straight
        // res.attachment('Quản lý ô nhiễm Data.xlsx') // This is sails.js specific (in general you need to set headers)
        // return res.send(report)
        res.json({ buffer: report, fileName: 'Quản lý ô nhiễm Data.xlsx'})
      } catch (e) {
        res.json({
          error: true,
          message: JSON.stringify(e)
        })
        throw e
      }
    })

    router.get('/getAll', async (req, res) => {
      try {
        let query = _.pick(req.query, this.queryProps)
        if (this.fieldNameSearch && req.query.search) {
          query = {
            ...query,
            $or: [
              {
                [this.fieldNameSearch]: { $regex: `${req.query.search}` }
              },
              {
                FacilityCode: { $regex: `${req.query.search}` }
              }
            ]
          }
        }
        const totalItem = await facilityemissionDao.getTotalCount(query)

        let field = []
        if (req.query.field && req.query.field.length > 0)
          field = req.query.field

        const itemList = await FacilityemissionSimpleModel.find(
          {},
          {
            facility: 1,
            emissionType: 1,
            IssueDate: 1,
            AddedOn: 1,
            DataValue: 1
          }
        )
          .populate('facility')
          .lean()
        const to = moment()

        res.json({ success: true, data: itemList, totalItem })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

     //Loger
     router.post('/', async (req, res, next) => {
      try {
        let item = req.body
        let facility = await Facility.findOne({_id: item.facility })
        let TenCoSo = facility? facility.FacilityNameFull : ""
        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.CREATE,
          LogEvent: `Thêm mới Quản lý ô nhiễm:
          Cơ sở: ${TenCoSo}
          ...
          `
        })
        next()
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.put('/:_id', async (req, res, next) => {
      try {
        let item = req.body
        let facility = await Facility.findOne({_id: item.facility })
        let TenCoSo = facility? facility.FacilityNameFull : ""
        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.UPDATE,
          LogEvent: `Chỉnh sửa Quản lý ô nhiễm:
          _id: ${req.params._id}
          Cơ sở: ${TenCoSo}
          ...
          `
        })
        next()
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.delete('/:_id', async (req, res, next) => {
      try {
        let item = await facilityemissionDao.findOne({ _id: req.params._id})
        let facility = await Facility.findOne({_id: item.facility })
        let TenCoSo = facility? facility.FacilityNameFull : ""
        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.DELETE,
          LogEvent: `Xoá Quản lý ô nhiễm:
          _id: ${req.params._id}
          Cơ sở: ${TenCoSo}
          ...
          `
        })
        next()
      } catch (e) {
        res.json({error: true, message: e.message})
        throw e
      }
    })
     //END Loger

    super(facilityemissionDao, router)
    this.queryProps = ['facility', 'sector','district','industrialArea', 'authority','emissionType']
    this.updateProps = [
      'Name',
      'DataValue',
      'Description',
      'emissionType',
      'facility'
    ]
    this.fieldNameSearch = 'FacilityNameFull'
  }
}

export default new FacilityemissionRoute().router
