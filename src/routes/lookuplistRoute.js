import lookuplistDao from 'dao/lookuplistDao'
import BaseRoute from './baseRoute'
import { resError } from 'utils/res'
import Lookuplist from 'models/Lookuplist'
import express from 'express'
const router = express.Router()

export class LookuplistRoute extends BaseRoute {
  constructor() {
    router.get('/getAll', async (req, res) => {
      try {
        const itemList = await Lookuplist.find()

        res.json({ success: true, data: itemList, totalItem: itemList.length })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    super(lookuplistDao, router)
    // this.queryProps = ['Name','ProvinceID']
    // this.updateProps = ['Name', 'DisplaySequence']
  }
}

export default new LookuplistRoute().router
