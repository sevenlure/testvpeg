import facilityattachmentDao from 'dao/facilityattachmentDao'
import BaseRoute from './baseRoute'

export class FacilityattachmentRoute extends BaseRoute {
  constructor() {
    super(facilityattachmentDao)
    this.queryProps = ['facility']
    this.updateProps = [
   
    ]
    //this.fieldNameSearch = 'FacilityNameFull'
  }
}

export default new FacilityattachmentRoute().router
