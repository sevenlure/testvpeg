import provinceDao from 'dao/provinceDao'
import BaseRoute from './baseRoute'

export class ProvinceRoute extends BaseRoute {
  constructor() {
    super(provinceDao)
    this.queryProps = ['NameLong']
    this.updateProps = ['CentreLat', 'CentreLong']
  }
}

export default new ProvinceRoute().router
