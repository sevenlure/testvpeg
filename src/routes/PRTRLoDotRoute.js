import PRTRLoDotDao from 'dao/PRTRLoDotDao'
import PRTRLoDotConfigDao from 'dao/PRTRLoDotConfigDao'
import BaseRoute from './baseRoute'
import { resError } from 'utils/res'
import { get as _get } from 'lodash'
import PRTRLoDot from 'models/PRTRLoDot'
import express from 'express'

const router = express.Router()
export class PRTRRoute extends BaseRoute {
  constructor() {
    router.get('/getAll', async (req, res) => {
      try {
        const itemList = await PRTRLoDot.find()

        res.json({ success: true, data: itemList, totalItem: itemList.length })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    super(PRTRLoDotDao, router)
    this.queryProps = ['facility', 'sector', 'AddedBy', 'isSend', 'YYYY', 'MM']
    this.updateProps = [
      'facility',
      'typeOfWaste',
      'levelProcess',
      'config',
      'name',
      'Description',
      'capacityDesign',
      'capacityOperating',
      'mass',
      'outSite',
      'intSite',
      'khongKhi',
      'troBay',
      'xiLo',
      'isSend',
      'sendAt',
      'maDN',
      'YYYY',
      'MM'
    ]

    router.post('/config', async (req, res) => {
      try {
        let item = req.body
        let record = await PRTRLoDotConfigDao.create({
          ...item,
          AddedBy: req.user ? req.user.UserName : undefined
        })
        res.json({ success: true, data: record })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.get('/config/getAll', async (req, res) => {
      try {
        const item = await PRTRLoDotConfigDao.getList({}, {})
        res.json({ success: true, data: item, totalItem: item.length })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.get('/config/:_id', async (req, res) => {
      try {
        const query = {}
        // console.log(req.params._id)
        // console.log("wwww")
        query._id = _get(req, ['params', '_id'], null)
        const item = await PRTRLoDotConfigDao.findOne(query)
        res.json({ success: true, data: item })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })
  }
}

export default new PRTRRoute().router
