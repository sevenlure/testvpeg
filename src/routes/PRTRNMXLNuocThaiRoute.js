import PRTRNMXLNuocThaiDao from 'dao/PRTRNMXLNuocThaiDao'
import BaseRoute from './baseRoute'
import { resError } from 'utils/res'
import PRTRNMXLNuocThai from 'models/PRTRNMXLNuocThai'
import express from 'express'

const router = express.Router()
export class PRTRNMXLNuocThaiRoute extends BaseRoute {
  constructor() {
    router.get('/getAll', async (req, res) => {
      try {
        const itemList = await PRTRNMXLNuocThai.find()

        res.json({ success: true, data: itemList, totalItem: itemList.length })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })
    router.post('/', async (req, res) => {
      try {
        let item = req.body
        const objExist = await PRTRNMXLNuocThai.findOne({
          facility: _.get(req.body, 'facility'),
          'YYYY.value': _.get(req.body, 'YYYY.value'),
          'MM.value': _.get(req.body, 'MM.value')
        })
        if (objExist) {
          resError(res, `Đợt ${_.get(req.body, 'YYYY.value')}/${_.get(req.body, 'MM.value')}này đã tồn tại. Vui lòng kiểm tra lại`)
        }
        let record = await PRTRNMXLNuocThai.create({
          ...item,
          AddedBy: req.user ? req.user.UserName : undefined
        })
        res.json({ success: true, data: record })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    super(PRTRNMXLNuocThaiDao, router)
    this.queryProps = ['facility', 'sector', 'isSend', 'YYYY', 'MM']
    this.updateProps = [
      'facility',
      'capacityDesign',
      'capacityOperating',
      'nongDoSauXuLy',
      'sanPhoiBun',
      'mayEpBun',
      'langTrongLuc',
      'mayLyTam',
      'xuLyTaiCoSo',
      'chuyenGiao',
      'khongKhi',
      'nuoc',
      'isSend',
      'maDN',
      'YYYY',
      'MM',
      'donViPhanTich',
      'ngayLayMau',
      'ngayTraKetQua',
      'sendAt'
    ]
  }
}

export default new PRTRNMXLNuocThaiRoute().router
