import eiaattachmentDao from 'dao/eiaattachmentDao'
import BaseRoute from './baseRoute'

export class EiaattachmentRoute extends BaseRoute {
  constructor() {
    super(eiaattachmentDao)
    this.queryProps = ['facilityeia']
    this.updateProps = [
   
    ]
    //this.fieldNameSearch = 'FacilityNameFull'
  }
}

export default new EiaattachmentRoute().router
