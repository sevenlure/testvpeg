import userDao from 'dao/userDao'
import User from 'models/User'
import BaseRoute from './baseRoute'
import { resError } from 'utils/res'
import express from 'express'
import { connection } from 'mongoose'
// import fs from 'fs'
import bcrypt from 'bcrypt'
import { JWT_SECRET } from 'config'
import jwt from 'jsonwebtoken'
import Log, {TYPE_EVENT} from 'models/Log'
import Facility from 'models/Facility'

const SALTS = 10

const router = express.Router()

export class UserRoute extends BaseRoute {
  constructor() {
    router.post('/', async (req, res, next) => {
      try {
        let item = req.body
        item.Password = await bcrypt.hash(item.Password, SALTS)
        console.log('item.Password', item.Password)
        next()
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.put('/userChangePassword/:_id', async (req, res, next) => {
      try {
        let user = await User.findById(req.params._id)
        if (!user) return res.json({ error: true, message: 'K0 tìm thấy user' })
        let isTruePassword = await bcrypt.compare(
          req.body.Password,
          user.Password
        )
        if (!isTruePassword)
          return res.json({ error: true, message: 'Mật khẩu hiện tại k0 đúng' })
        user = await User.findOneAndUpdate(
          {
            _id: req.params._id
          },
          {
            $set: {
              Password: await bcrypt.hash(req.body.NewPassword, SALTS),
              UpdatedOn: Date.now()
            }
          }
        )

        res.json({
          success: true,
          data: user,
          message: 'Change password thành công'
        })

        next()
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.put('/changePassword/:_id', async (req, res, next) => {
      try {
        let item = req.body
        let NewPassword = await bcrypt.hash(item.Password, SALTS)

        let user = await User.findOneAndUpdate(
          {
            _id: req.params._id
          },
          {
            $set: {
              Password: NewPassword,
              UpdatedOn: Date.now()
            }
          }
        )

        res.json({
          success: true,
          data: user,
          message: 'Change password thành công'
        })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.get('/getAll', async (req, res) => {
      try {

        const itemList = await User.find(
          {},
          {
            UserName: 1,
          }
        )

        res.json({ success: true, data: itemList })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    //Loger
    router.post('/', async (req, res, next) => {
      try {
        let item = req.body
        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.CREATE,
          LogEvent: `Thêm mới Nguời dùng:
          UserName: ${item.UserName}
          FirstName: ${item.FirstName}
          LastName: ${item.LastName}
          Email: ${item.Email}
          ...
          `
        })
        next()
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.put('/:_id', async (req, res, next) => {
      try {
        let item = req.body
        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.CREATE,
          LogEvent: `Chỉnh sửa Nguời dùng:
          UserName: ${item.UserName}
          FirstName: ${item.FirstName}
          LastName: ${item.LastName}
          Email: ${item.Email}
          ...
          `
        })
        next()
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.delete('/:_id', async (req, res, next) => {
      try {
        let item = await userDao.findOne({ _id: req.params._id})        

        Log.create({
          user: req.user ? req.user._id : undefined,
          UserName: req.user ? req.user.UserName : undefined,
          TypeEvent: TYPE_EVENT.CREATE,
          LogEvent: `Xoá Nguời dùng:
          UserName: ${item.UserName}
          FirstName: ${item.FirstName}
          LastName: ${item.LastName}
          Email: ${item.Email}
          ...
          `
        })
        next()
      } catch (e) {
        res.json({error: true, message: e.message})
        throw e
      }
    })
     //END Loger

    const notInherit = {
      isEdit: true
    }

    super(userDao, router, notInherit)
    this.queryProps = ['district', 'StatusCode', 'securityLevel', 'FirstName']
    this.updateProps = [
      'district',
      'StatusCode',
      'securityLevel',
      'FirstName',
      'LastName',
      'Email',
      'Telephone',
      'Fax',
      'Organization',
      'LastAccessed',
      'role',
      'facility',
      'sector'
    ]
    this.fieldNameSearch = ''

    this.router.put('/:_id', async (req, res) => {
      try {
        const query = { _id: req.params._id }
        let itemExist = await this.objDao.findOne(query)
        if (!itemExist) resError(res, 'ITEMS_NOT_EXISTS')
        const itemData = _.pick(req.body, this.updateProps)
        let record = await this.objDao.update(query, {
          ...itemData,
          UpdatedBy: req.user ? req.user.UserName : undefined
        })
        const token = record.toObject().TokenLast
        if (token) {
          dbMyJsonDataBase.push(
            BLACKLIST_JSON_PATH,
            { [record.toObject().UserName]: token },
            false
          )
        }

        res.json({ success: true, data: record })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })
  }
}

export default new UserRoute().router
