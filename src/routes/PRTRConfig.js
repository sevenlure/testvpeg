import lookuplistitemDao from 'dao/lookuplistitemDao'
import BaseRoute from './baseRoute'
import { resError } from 'utils/res'
import express from 'express'
import { getPagination } from 'utils/pagination'
import { map as _map, pick as _pick } from 'lodash'
const router = express.Router()

export class PRTRConfigRoute extends BaseRoute {
  constructor() {
    router.put('/updateListItemOptions', async (req, res) => {
      try {
        const arrayData = req.body
        if (!arrayData || arrayData.length === 0)
          resError(res, 'ITEMS_NOT_EXIST')

        let funcPromiseArr = _map(arrayData, item => {
          const query = { _id: item._id }
          const itemData = _.pick(item, this.updateProps)
          let record = lookuplistitemDao.update(query, {
            ...itemData,
            UpdatedBy: req.user ? req.user.UserName : undefined
          })
          return record
        })
        const result = await Promise.all(funcPromiseArr)

        res.json({ success: true, data: result })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    router.get('/:key', async (req, res) => {
      try {
        let query =  _.pick(req.query, this.queryProps)
        query[`Options.${req.params.key}`]  = true
        const totalItem = await lookuplistitemDao.getTotalCount(query)
        // console.log(totalItem,"totalItem")
        const pagination = getPagination(req, totalItem)

        let field = []
        if (req.query.field && req.query.field.length > 0)
          field = req.query.field

        // console.log(query)
        const itemList = await lookuplistitemDao.getList(
          query,
          pagination,
          field
        )

        res.json({ success: true, data: itemList, pagination })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

    super(lookuplistitemDao, router)
    this.queryProps = ['ProvinceID', 'LookupListID', 'Options.loDot']
    this.updateProps = [
      'Key',
      'Name',
      'DisplaySequence',
      'ExtraInfo',
      'Options'
    ]
  }
}

export default new PRTRConfigRoute().router
