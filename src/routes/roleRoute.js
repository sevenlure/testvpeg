import User from 'models/User'
import roleDao from 'dao/roleDao'
import BaseRoute from './baseRoute'
import { resError } from 'utils/res'
import express from 'express'
import {connection} from 'mongoose'


const router = express.Router()
export class RoleRoute extends BaseRoute {
  constructor() {

    // router.post('/', async (req, res, next) => {
    //   try {
    //     let item = req.body
    //     item.Password =  await bcrypt.hash(item.Password, SALTS)
    //     console.log('item.Password',item.Password)
    //     next()
    //   } catch (e) {
    //     resError(res, e.message)
    //     throw e
    //   }
    // })

    const notInherit = {
      isEdit: true
    }
    
    super(roleDao, router, notInherit)
    this.queryProps = [
      'Name',
      'Key',
      'permissionRole'
    ]
    this.updateProps = [
      'permissionRole'
    ]
    this.fieldNameSearch = ''

    this.router.put('/:_id', async (req, res) => {
      try {
        const query = { _id: req.params._id }
        let itemExist = await this.objDao.findOne(query)
        if (!itemExist) resError(res, 'ITEMS_NOT_EXISTS')
        const itemData = _.pick(req.body, this.updateProps)
        let record = await this.objDao.update(query, {
          ...itemData,
          UpdatedBy: req.user ? req.user.UserName : undefined
        })

        let userDataInRole = await User.find({
          role: req.params._id
        }).lean()

        if(userDataInRole && userDataInRole.length > 0){
           userDataInRole.forEach(item => {
            dbMyJsonDataBase.push(
              BLACKLIST_JSON_PATH,
              { [item.UserName]: item.TokenLast },
              false
            )
          });
        }
        
        res.json({ success: true, data: record })
      } catch (e) {
        resError(res, e.message)
        throw e
      }
    })

  }

}

export default new RoleRoute().router
