import express from 'express'
import { resError, resSuccess } from 'utils/res'
import PRTRLoDotDao from 'dao/PRTRLoDotDao'
import PRTRNMXLNuocThaiDao from 'dao/PRTRNMXLNuocThaiDao'
import PRTRReportDao from 'dao/PRTRReportDao'
import {
  get as _get,
  compact as _compact,
  map as _map,
  toNumber as _toNumber,
  isEmpty as _isEmpty
} from 'lodash'
import path from 'path'
import appRoot from 'app-root-path'
import { Schema } from 'mongoose'

const router = express.Router()

router.get('/download/:fileName', (req, res) => {
  res.download(path.join(appRoot.path, 'output', req.params.fileName))
})

/* #region  report lo dot */

router.get('/incinerator-detail/:_id', async (req, res) => {
  try {
    const query = {}
    // console.log(req.params._id)
    // console.log(req.query)
    query._id = _get(req, ['params', '_id'], null)
    const item = await PRTRLoDotDao.findOne(query)
    resSuccess(res, { data: item })
  } catch (e) {
    resError(res, e.message)
    throw e
  }
})
router.get('/incinerator-by-facility/:_id', async (req, res) => {
  try {
    let query = {}
    if (!_isEmpty(req.query)) {
      query = {
        ...query,
        'MM.value': _toNumber(_get(req, 'query.MM', null)),
        'YYYY.value': _toNumber(_get(req, 'query.YYYY', null)),
        isSend: true
      }
    }
    query.facility = _get(req, ['params', '_id'], null)
    // console.log(query)
    const itemList = await PRTRLoDotDao.getList(query, {})
    resSuccess(res, { data: itemList, totalItem: itemList.length })
  } catch (e) {
    resError(res, e.message)
    throw e
  }
})

/* #endregion */

/* #region  report nmxlnuocthai */
router.get('/water-treatment-factory-detail/:_id', async (req, res) => {
  try {
    const query = {}
    query._id = _get(req, ['params', '_id'], null)
    // console.log(query,"query")

    const itemList = await PRTRNMXLNuocThaiDao.findOne(query)
    resSuccess(res, { data: itemList, totalItem: itemList.length })
  } catch (e) {
    resError(res, e.message)
    throw e
  }
})
/* #endregion */

/* #region  report nmxlnuocthai */
router.get('/water-treatment-factory-detail/:_id', async (req, res) => {
  try {
    const query = {}
    query._id = _get(req, ['params', '_id'], null)
    // console.log(query,"query")

    const itemList = await PRTRNMXLNuocThaiDao.findOne(query)
    resSuccess(res, { data: itemList, totalItem: itemList.length })
  } catch (e) {
    resError(res, e.message)
    throw e
  }
})
/* #endregion */

/* #region  cac bao cao theo da nganh và theo PPTT */
router.get('/general/:_id', async (req, res) => {
  try {
    let query = {}

    const ArrParams = _compact(_get(req, 'params._id', null).split('-'))
    const PPTT = _get(req, 'query.PPTT', null)
    query = {
      $and: [
        {
          sector: {
            $in: ArrParams
          }
        },
        {
          $or: [
            {
              'prtrlodots_docs.isSend': true
            },
            {
              'prtrNuocThai_docs.isSend': true
            }
          ]
        },
        {
          $or: [
            {
              'prtrlodots_docs.MM.value': _toNumber(
                _get(req, 'query.MM', null)
              ),
              'prtrlodots_docs.YYYY.value': _toNumber(
                _get(req, 'query.YYYY', null)
              )
            },
            {
              'prtrNuocThai_docs.MM.value': _toNumber(
                _get(req, 'query.MM', null)
              ),
              'prtrNuocThai_docs.YYYY.value': _toNumber(
                _get(req, 'query.YYYY', null)
              )
            }
          ]
        }
      ]
    }
    // console.log(query, PPTT)
    const itemList = await PRTRReportDao.reportGeneral(query, PPTT)
    resSuccess(res, { data: itemList, totalItem: itemList.length })
  } catch (e) {
    resError(res, e.message)
    throw e
  }
})
router.get('/general-excel/:_id', async (req, res) => {
  try {
    let query = {}

    const ArrParams = _compact(_get(req, 'params._id', null).split('-'))
    const PPTT = _get(req, 'query.PPTT', null)
    query = {
      $and: [
        {
          sector: {
            $in: ArrParams
          }
        },
        {
          $or: [
            {
              'prtrlodots_docs.isSend': true
            },
            {
              'prtrNuocThai_docs.isSend': true
            }
          ]
        }
      ],

      $or: [
        {
          'prtrlodots_docs.MM.value': _toNumber(_get(req, 'query.MM', null)),
          'prtrlodots_docs.YYYY.value': _toNumber(_get(req, 'query.YYYY', null))
        },
        {
          'prtrNuocThai_docs.MM.value': _toNumber(_get(req, 'query.MM', null)),
          'prtrNuocThai_docs.YYYY.value': _toNumber(
            _get(req, 'query.YYYY', null)
          )
        }
      ]
    }
    const itemList = await PRTRReportDao.reportGeneral(query, PPTT)
    if (itemList) {
      const stylecolumn = {
        width: 20,
        style: { font: { name: 'Times New Roman', size: 14 } }
      }
      const columns = [
        { key: 'stt', ...stylecolumn, width: 5 },
        { key: 'tenChat', ...stylecolumn, width: 30 },
        { key: 'pptt', ...stylecolumn, width: 30 },
        { key: 'sodn', ...stylecolumn },
        { key: 'khi', ...stylecolumn },
        { key: 'nuoc', ...stylecolumn },
        { key: 'ctr', ...stylecolumn },
        { key: 'bun', ...stylecolumn },
        { key: 'tong', ...stylecolumn },
        { key: 'taiCoSo_taiChe', ...stylecolumn },
        { key: 'taiCoSo_xuly', ...stylecolumn },
        { key: 'taiCoSo_tong', ...stylecolumn },
        { key: 'ngoaiCoSo_taiChe', ...stylecolumn },
        { key: 'ngoaiCoSo_xuly', ...stylecolumn },
        { key: 'ngoaiCoSo_tong', ...stylecolumn }
      ]
      const data = await PRTRReportDao.processGenExcel(
        itemList,
        'BC_Tong_Hop',
        'report-template-tong-hop.xlsx',
        columns
      )
      if (!data.error) {
        resSuccess(res, data)
      } else {
        resError(res, data.err)
      }
    }
  } catch (e) {
    resError(res, e.message)
    throw e
  }
})

router.get('/general-by-sulphite/:_id', async (req, res) => {
  try {
    let query = {}
    const ArrParams = _compact(_get(req, 'params._id', null).split('-'))
    const PPTT = _get(req, 'query.PPTT', null)

    query = {
      sector: {
        $in: ArrParams
      },
      $or: [
        {
          'prtrlodots_docs.MM.value': _toNumber(_get(req, 'query.MM', null)),
          'prtrlodots_docs.YYYY.value': _toNumber(_get(req, 'query.YYYY', null))
        },
        {
          'prtrNuocThai_docs.MM.value': _toNumber(_get(req, 'query.MM', null)),
          'prtrNuocThai_docs.YYYY.value': _toNumber(
            _get(req, 'query.YYYY', null)
          )
        }
      ]
    }

    let hoaChat = _get(req, 'query.hoaChat', [])
    if (hoaChat) {
      hoaChat = JSON.parse(hoaChat)
    }

    const itemList = await PRTRReportDao.reportGeneralBySulphite(
      query,
      PPTT,
      hoaChat
    )
    resSuccess(res, { data: itemList, totalItem: itemList.length })
  } catch (e) {
    resError(res, e.message)
    throw e
  }
})

router.get('/general-by-sulphite-excel/:_id', async (req, res) => {
  try {
    let query = {}
    const ArrParams = _compact(_get(req, 'params._id', null).split('-'))
    const PPTT = _get(req, 'query.PPTT', null)

    query = {
      sector: {
        $in: ArrParams
      },
      $or: [
        {
          'prtrlodots_docs.MM.value': _toNumber(_get(req, 'query.MM', null)),
          'prtrlodots_docs.YYYY.value': _toNumber(_get(req, 'query.YYYY', null))
        },
        {
          'prtrNuocThai_docs.MM.value': _toNumber(_get(req, 'query.MM', null)),
          'prtrNuocThai_docs.YYYY.value': _toNumber(
            _get(req, 'query.YYYY', null)
          )
        }
      ]
    }

    let hoaChat = _get(req, 'query.hoaChat', [])
    hoaChat = JSON.parse(hoaChat)

    const itemList = await PRTRReportDao.reportGeneralBySulphite(
      query,
      PPTT,
      hoaChat
    )
    // resSuccess(res, { data: itemList, totalItem: itemList.length })
    if (itemList) {
      const stylecolumn = {
        width: 20,
        style: { font: { name: 'Times New Roman', size: 14 } }
      }
      const columns = [
        { key: 'stt', ...stylecolumn, width: 5 },
        { key: 'maDN', ...stylecolumn, width: 30 },
        { key: 'tenDN', ...stylecolumn, width: 60 },
        { key: 'PhanTich', ...stylecolumn, width: 30 },
        { key: 'EF', ...stylecolumn, width: 30 },
        { key: 'DNNhap', ...stylecolumn, width: 30 },
        { key: 'tenNganh', ...stylecolumn, width: 60 }
      ]
      const data = await PRTRReportDao.processGenExcel(
        itemList,
        'BC_Tong_Hop_HoaChat',
        'report-template-tong-hop-hoa-chat.xlsx',
        columns
      )
      if (!data.error) {
        resSuccess(res, data)
      } else {
        resError(res, data.err)
      }
    }
  } catch (e) {
    resError(res, e.message)
    throw e
  }
})
/* #endregion */

export default router
