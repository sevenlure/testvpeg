import Errors from 'constants/errors'
import { resError } from 'utils/res'
import Lookuplistitem from 'models/Lookuplistitem'

export default async function facilityRoleMiddleware(req, res, next) {
  const roleKey = _.result(req, 'user.role.Key', 'GUEST')

  // if (roleKey === 'DISTRICT_USER') {
  //   let authorItem = await Lookuplistitem.findOne({
  //     Key: 'DISTRICT'
  //   })
  //   req.query.authority = _.result(authorItem, '_id', '')
  // }

  // if (roleKey === 'PROVINCE_USER') {
  //   let authorItem = await Lookuplistitem.findOne({
  //     Key: 'PROVINCE'
  //   })
  //   req.query.authority = _.result(authorItem, '_id', '')
  // }

    if (roleKey === 'INDUSTRIAL_USER') {
      req.query.industrialArea = {
        $ne: null
      }
  }

  //Check query BenTrong BenNgoai
  if(req.query.bentrongngoaiSelect && !req.query.industrialArea){
    if(req.query.bentrongngoaiSelect === 'Inside'){
      req.query.industrialArea = {
        $ne: null
      }
    }else{
      req.query.industrialArea = null
    }
  }

  // console.log(req.query)
  next()
}
