import authDao from 'dao/authDao'
import Errors from 'constants/errors'
import { resError } from 'utils/res'
import { JWT_SECRET } from 'config'
import jwt from 'jsonwebtoken'

function getAuthorizationToken(req) {
  return req.headers['authorization'] || req.query.token || req.body.token
}

export default async function authMiddleware(req, res, next) {
  let authToken = getAuthorizationToken(req)
  let typeError = 'AUTH'
  let error = Errors.NOT_AUTHENTICATED
  if (authToken) {
    let user
  
    try {
      user = jwt.verify(authToken, JWT_SECRET)
    } catch (err) {
      if(err.name ==="TokenExpiredError"){
        error = Errors.TOKEN_EXPIRE
      }
      console.log('useraaa', err)
    }

    
    //Check token co blackList
    if (user) {
      let blackListData = await dbMyJsonDataBase.getData(BLACKLIST_JSON_PATH)
      let isBlackList = blackListData[user.UserName] === authToken
      if (isBlackList) {
        user = null
        error = Errors.USER_OR_ROLE_CHANGED
      }
    }
    if (!user) {
      resError(res, error, typeError)
    } else {
      // If user login, append `user` to req
      req.user = user
      //res.json({})
      next()
    }
  } else {
    resError(res, error, typeError)
  }
  //next()
}
