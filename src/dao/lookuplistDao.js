import BaseDao from './baseDao'
import Lookuplist from 'models/Lookuplist'

export class LookuplistDao extends BaseDao {
  constructor() {
    super(Lookuplist)
    this.sort = {
      // DisplaySequence: 1
    }
  }
}

export default new LookuplistDao()
