import BaseDao from './baseDao'
import User from 'models/User'

export class UserDao extends BaseDao {
  constructor() {
    super(User)
    this.sort={
      AddedOn: -1
    }
  }
}

export default new UserDao()