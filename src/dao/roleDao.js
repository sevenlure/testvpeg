import BaseDao from './baseDao'
import Role from 'models/Role'

export class RoleDao extends BaseDao {
  constructor() {
    super(Role)
    this.sort = {
      Order: 1
    }
  }

  async getList(query = {}, { minIndex, itemPerPage }, field = []) {
    let data
    if (minIndex != null && itemPerPage) {
      data = await this.Model.find({ ...query, Key: { $ne: 'ADMIN' } }, field, {
        sort: this.sort,
        skip: minIndex,
        limit: itemPerPage
      })
    } else {
      data = await this.Model.find(query, field, {
        sort: this.sort
      })
    }
    return data
  }
}

export default new RoleDao()
