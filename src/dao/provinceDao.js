import BaseDao from './baseDao'
import Province from 'models/Province'

export class ProvinceDao extends BaseDao {
  constructor() {
    super(Province)
  }
}

export default new ProvinceDao()
