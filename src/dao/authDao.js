import User from 'models/User'
import bcrypt from 'bcrypt'
import jsonWebToken from 'jsonwebtoken'
import { JWT_SECRET } from 'config'
import { buildCleaner } from 'lodash-clean'

const options = {
  isBoolean: val => {
    return val || undefined
  }
}

const clean = buildCleaner(options)
const SALTS = 10

// const rolePaths = _.keys(flatObject(permissionRole));

function flatObject(input) {
  function flat(res, key, val, pre = '') {
    const prefix = [pre, key].filter(v => v).join('.')
    return typeof val === 'object'
      ? Object.keys(val).reduce(
          (prev, curr) => flat(prev, curr, val[curr], prefix),
          res
        )
      : Object.assign(res, { [prefix]: val })
  }

  return Object.keys(input).reduce(
    (prev, curr) => flat(prev, curr, input[curr]),
    {}
  )
}

export default {
  /**
   * Check user exists email before register
   * @param UserName
   * @returns {boolean}
   */
  async checkExistsByUserName(UserName) {
    return !!(await User.findOne({ UserName }))
  },

  /**
   * Register user
   * @param UserName
   * @param Password
   * @returns {*|Promise}
   */
  async register({ UserName, Password }) {
    let user = new User({
      UserName,
      Password: await bcrypt.hash(Password, SALTS)
    })
    await user.save()
    // save to database
    return user
  },

  /**
   * Login
   * @param UserName
   * @param Password
   * @returns {*}
   */
  async login({ UserName, Password }) {
    let user = await User.findOne({ UserName })
    if (!user) return null
    let isLogin = await bcrypt.compare(Password, user.Password)
    return isLogin ? user : null
  },

  /**
   * Create token from user object
   * @param user
   * @returns {*}
   */
  async createToken(user) {
    // Generate token
    let permissionRole = clean(_.result(user, 'role.permissionRole', {}))
    var token = jsonWebToken.sign(
      {
        _id: user._id,
        UserName: user.UserName,
        role: user.role,
        permissionRole: permissionRole? _.keys(flatObject(permissionRole)) : [],
        district: _.result(user, 'district._id', '')
      },
      JWT_SECRET,
      {
        expiresIn: '30m'
      }
    )
    const updateTokenUser = await User.findByIdAndUpdate(user._id, {
      $set: {
        TokenLast: token
      }
    })
    return token
  },

  /**
   * From token to user
   * @param token
   * @returns {boolean}
   */
  async getUserFromToken(token) {
    try {
      // De code token
      let dataDecoded = await jsonWebToken.verify(token, JWT_SECRET)
      if (dataDecoded._id) {
        const user = await User.findOne({ _id: dataDecoded._id })
        return user || false
      }
      return false
    } catch (e) {
      return false
    }
  }
}
