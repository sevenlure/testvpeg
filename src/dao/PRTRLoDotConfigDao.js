import BaseDao from './baseDao'
import PRTRLoDotConfig from 'models/PRTRLoDotConfig'

export class PRTRLoDotConfigDao extends BaseDao {
  constructor() {
    super(PRTRLoDotConfig)
    this.sort={
      AddedOn: -1
    }
  }
}

export default new PRTRLoDotConfigDao()