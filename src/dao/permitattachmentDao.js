import BaseDao from './baseDao'
import Permitattachment from 'models/Permitattachment'
import Attachments from 'models/Attachments'
import mongoose from 'mongoose'

export class PermitattachmentDao extends BaseDao {
  constructor() {
    super(Permitattachment)
  }

  async create(obj) {
    const Model = this.Model

    const attachment = new Attachments({
      ...obj,
      _id: new mongoose.Types.ObjectId()
    })
    let res = await attachment.save()
    //console.log(res)

    const item = new Model({
      ...obj,
      attachment: attachment._id
    })
    await item.save()

    return item
  }

  async updateAttachment(query, newObj) {
    const item = await Attachments.findOneAndUpdate(
      query,
      {
        $set: {
          ...newObj,
          UpdatedOn: Date.now()
        }
      },
      { new: true }
    )
    return item
  }


  async delete(query) {
    const item = await this.Model.findOne(query)
    const resRemove = await this.Model.remove(query)
    //console.log(item.attachment)
    await this.updateAttachment({
      _id: item.attachment._id
    },{
      isRemove: true
    })
    return resRemove
  }
}

export default new PermitattachmentDao()
