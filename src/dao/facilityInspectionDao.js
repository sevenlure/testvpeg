import BaseDao from 'dao/baseDao'
import FacilityInspection from 'models/FacilityInspection'
import Facility from 'models/Facility'

export class FacilityInspectionDao extends BaseDao {
  constructor() {
    super(FacilityInspection)
    this.sort = {
      AddedOn: -1
    }
    this.pipeline = [
      {
        $lookup: {
          from: 'facilities',
          localField: 'facility',
          foreignField: '_id',
          as: 'facility'
        }
      },
      {
        $unwind: {
          path: '$facility',
          preserveNullAndEmptyArrays: false
        }
      },
      {
        $addFields: {
          sector: {
            $toString: '$facility.sector'
          },
          district: {
            $toString: '$facility.district'
          },
          industrialArea: {
            $toString: '$facility.industrialArea'
          },
          authority: {
            $toString: '$facility.authority'
          },
          inspectionStatus: {
            $toString: '$inspectionStatus'
          },
          FacilityNameFull: '$facility.FacilityNameFull',
          FacilityCode: '$facility.FacilityCode',
          facility: '$facility._id'
        }
      },
      { $sort: this.sort }
    ]
  }

  async getTotalCount(query = {}) {
    let data = await this.Model.aggregate([
      ...this.pipeline,
      {
        $match: query
      }
    ]).count('count')
    if (data.length > 0) return data[0].count
    else return 0
  }

  async getList(query = {}, { minIndex, itemPerPage }, field = []) {
    let data
    if (minIndex != null && itemPerPage) {
      data = await this.Model.aggregate([
        ...this.pipeline,
        {
          $match: query
        },
        { $skip: minIndex },
        { $limit: itemPerPage }
      ])
      await this.Model.populate(data, { path: 'facility' })
      await this.Model.populate(data, { path: 'inspectionStatus' })
      await this.Model.populate(data, { path: 'purposeObj' })
      await this.Model.populate(data, { path: 'complianceType' })
      return data
    } else {
      data = await this.Model.aggregate([
        ...this.pipeline,
        {
          $match: query
        }
      ])
      return data
    }
  }

  async create(obj) {
    const Model = this.Model
    const item = new Model(obj)
    await item.save()
    await Facility.findByIdAndUpdate(
      item.facility,
      { "$push": { "facilityinspections": item._id } },
      { "new": true, "upsert": true }
    )
    return item
  }

  async delete(query) {
    const item = await this.Model.findOneAndRemove(query)

    await Facility.findByIdAndUpdate(
      item.facility,
      { "$pull": { "facilityinspections": item._id } }
    )

    return item
  }

}

export default new FacilityInspectionDao()
