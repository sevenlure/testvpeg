import BaseDao from './baseDao'
import PRTRLoDot from 'models/PRTRLoDot'

export class PRTRLoDotDao extends BaseDao {
  constructor() {
    super(PRTRLoDot)
    this.sort={
      AddedOn: -1
    }
  }

}

export default new PRTRLoDotDao()