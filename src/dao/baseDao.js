export default class BaseDao {
  constructor(Model) {
    this.Model = Model
    this.sort = {}
  }

  async getTotalCount(query = {}) {
    let data = await this.Model.find(query).countDocuments()
    return data
  }

  async getList(query = {}, { minIndex, itemPerPage }, field = []) {
    let data
    if (minIndex != null && itemPerPage) {
      data = await this.Model.find(
        query,
        field,
        {
          sort: this.sort,
          skip: minIndex,
          limit: itemPerPage
        }
      )
    } else {
      data = await this.Model.find(
        query,
        field,
        {
          sort: this.sort,
        }
      )
    }
    return data
  }

  async create(obj) {
    const Model = this.Model
    const item = new Model(obj)
    await item.save()
    return item
  }

  async findOne(query) {
    let item = await this.Model.findOne(query)
    return item
  }

  async update(query, newObj) {
    const item = await this.Model.findOneAndUpdate(
      query,
      {
        $set: {
          ...newObj,
          UpdatedOn: Date.now()
        }
      },
      { new: true }
    )
    return item
  }

  async delete(query) {
    const item = await this.Model.remove(query)
    return item
  }
}
