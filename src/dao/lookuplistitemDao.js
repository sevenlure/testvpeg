import BaseDao from './baseDao'
import Lookuplistitem from 'models/Lookuplistitem'

export class LookuplistitemDao extends BaseDao {
  constructor() {
    super(Lookuplistitem)
    this.sort = {
      DisplaySequence: 1
    }
  }
}

export default new LookuplistitemDao()
