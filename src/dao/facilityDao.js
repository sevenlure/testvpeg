import BaseDao from './baseDao'
import Facility from 'models/Facility'

export class FacilityDao extends BaseDao {
  constructor() {
    super(Facility)
    this.sort={
      AddedOn: -1
    }
  }
}

export default new FacilityDao()