import BaseDao from './baseDao'
import PRTRFacility from 'models/PRTRFacility'
import mongoose from 'mongoose'
// import modelPRTRFacility from 'models/PRTRFacility'

import viewPRTRcoSo from 'models/view/viewPRTRcoSo'
import PRTRReportDao from 'dao/PRTRReportDao.js'
import {
  get as _get,
  sum as _sum,
  map as _map,
  concat as _concat,
  forEach as _forEach,
  pick as _pick,
  round as _round,
  compact as _compact,
  find as _find,
  reduce as _reduce,
  set as _set,
  values as _values,
  uniq as _uniq,
  omit as _omit,
  filter as _filter,
  keyBy as _keyBy
} from 'lodash'

export class PRTRFacilityDao extends BaseDao {
  constructor() {
    super(PRTRFacility)
    this.sort = {
      AddedOn: -1
    }
  }

  async getListCoSoPRTR(query = {}, hoaChat) {
    // console.log({ ...query }, '----------')
    const [res] = await Promise.all([
      viewPRTRcoSo.aggregate([
        {
          $match: {
            $and: [
              { ...query },
              {
                $or: [
                  {
                    'prtrlodots_docs.isSend': true
                  },
                  {
                    'prtrNuocThai_docs.isSend': true
                  }
                ]
              }
            ]
          }
        },
        { $lookup: {from: 'lookuplistitems', localField: 'sector', foreignField: '_id', as: 'sector'} },
        { $unwind: { path: '$sector', preserveNullAndEmptyArrays: true } },
        {
          $sort: {
            'prtrlodots_docs.YYYY.value': -1,
            'prtrlodots_docs.MM.value': -1,
            'prtrNuocThai_docs.YYYY.value': -1,
            'prtrNuocThai_docs.MM.value': -1
          }
        },
        {
          $project: {
            FacilityNameFull: 1,
            Latitude: 1,
            Longitude: 1,
            sector: 1,
            prtrlodots_docs: 1,
            prtrNuocThai_docs: 1
          }
        },
        {
          $group: {
            _id: '$_id',
            count: { $sum: 1 },
            child: {
              $addToSet: {
                _id: '$_id',
                FacilityNameFull: '$FacilityNameFull',
                Latitude: '$Latitude',
                Longitude: '$Longitude',
                sector: '$sector',
                MM: {
                  $ifNull: [
                    '$prtrlodots_docs.MM.value',
                    '$prtrNuocThai_docs.MM.value'
                  ]
                },
                YYYY: {
                  $ifNull: [
                    '$prtrlodots_docs.YYYY.value',
                    '$prtrNuocThai_docs.YYYY.value'
                  ]
                },
                prtrlodots_docs: '$prtrlodots_docs',
                prtrNuocThai_docs: '$prtrNuocThai_docs'
              }
            }
          }
        }
      ])
    ])

    if (!res) return
    // console.log(hoaChat,"--hoaChat--hoaChat")
   

    let resPRTRCoSo = []
    _forEach(res, item => {
      const MMMax = item.child[0].MM
      const YYYYMax = item.child[0].YYYY
      _forEach(item.child, itemLevel2 => {

        if(MMMax === itemLevel2.MM && YYYYMax === itemLevel2.YYYY){
          resPRTRCoSo.push({
            ...itemLevel2
          })
        }
        
      })
    })

    // return {
    //   as:resPRTRCoSo
    // }
    // console.log(resPhantich)
    const ArrPPTT = [
      { value: 'PhanTich', label: 'Đo đạc, phân tích' },
      { value: 'EF', label: 'Tính theo hệ số phát thải' }
      // { value: 'DNNhap', label: 'Doanh nghiệp nhập' }
    ]
    const resultTong = await PRTRReportDao.customDataTheoCongThuc(
      resPRTRCoSo,
      ArrPPTT
    )

    // console.log(resultTong)
    // console.log("------")
    // return resultTong

    const sumTheoChatvsPTTT = _reduce(
      _compact(resultTong),
      (prev, current) => {
        let hoaChats = _get(current, 'HoaChats', null)
        hoaChats = _filter(hoaChats, itemfillter => {
          const Name = _get(itemfillter, 'measuring.Name', null)
          // NOTE bỏ đi search theo tên request cần tìm
          // return Name === 'Dioxin/furan' || Name === 'Phenol'
          return Name === hoaChat
        })
        

        _forEach(hoaChats, item => {
          // console.log("----------")
          // console.log(hoaChats)
          const PRTRKey = _get(current, 'sector.PRTRKey', '')
          const PPTT = _get(item, 'PPTT.value', null)

          const Name = _get(current, 'FacilityNameFull', null) // _get(item, 'measuring.Name', null)
          const _id = `${Name}`
          const capacityOperatingCurrent = _get(current, 'capacityOperating')

          if (!prev[_id]) {
            prev[_id] = {} //item
            _set(prev[_id], ['measuring'], _get(item, 'measuring', null))
            _set(
              prev[_id],
              ['infoDoanhNhiep'],
              _pick(current, [
                '_id',
                'FacilityNameFull',
                'Latitude',
                'Longitude',
                'sector',
                'MM',
                'YYYY'
              ])
            )
            // return prev
          }

          // NOTE Cập nhật công suất
          if (capacityOperatingCurrent) {
            const prevCapacityOperatingTotal = _get(
              prev[_id],
              ['capacityOperatingTotal'],
              null
            )
            _set(
              prev[_id],
              ['capacityOperatingTotal'],
              prevCapacityOperatingTotal + capacityOperatingCurrent
            )
          }

          let isCal = false

          // NOTE Khối lượng chất thải đc tính dựa theo ngành nghề và phương pháp tính
          if (PRTRKey === 'loDot' && PPTT == 'PhanTich') {
            isCal = true
          } else if (PRTRKey === 'nmxlNuocThai' && PPTT == 'EF') {
            isCal = true
          }
          // console.log("run 1")
          // console.log(isCal)
          if (!isCal) {
            return
          }
          _set(prev[_id], ['PPTT'], _get(item, 'PPTT', null))

          // console.log(isCal)
          // console.log("---------")
          // console.log(item, PRTRKey, PPTT)

          const arrayKey = [
            'khi',
            'nuocThai',
            'troBay',
            'xiLo',
            'taiCoSo_taiChe',
            'taiCoSo_xuLy',
            'bun'
          ]

          _forEach(arrayKey, key => {
            let currentValue = 0
            let prevValue = 0
            /* #region  KHI THAI */
            currentValue = _get(item, [key], null)

            prevValue = _get(prev[_id], [key], null)
            // if(hoaChat === 'Dioxin/furan'){
            //   console.log("--------")
            //   console.log(prevValue,currentValue, item, key)
            // }

            if (currentValue) {
              _set(prev[_id], [key], prevValue + currentValue)
            }
          })
        })

        return prev
      },
      {}
    )

    // return MergeData
    return _values(sumTheoChatvsPTTT)
  }
}

export default new PRTRFacilityDao()
