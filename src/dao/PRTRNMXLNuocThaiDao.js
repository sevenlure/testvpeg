import BaseDao from './baseDao'
import PRTRNMXLNuocThai from 'models/PRTRNMXLNuocThai'

export class PRTRNMXLNuocThaiDao extends BaseDao {
  constructor() {
    super(PRTRNMXLNuocThai)
    this.sort={
      AddedOn: -1
    }
  }
}

export default new PRTRNMXLNuocThaiDao()