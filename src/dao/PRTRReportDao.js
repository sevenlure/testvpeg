import viewPRTRcoSo from 'models/view/viewPRTRcoSo'
import modelPRTRFacility from 'models/PRTRFacility'
import configNMXLNuocThai from 'constants/configPRTRnmxlNuocThai'
import PRTRLoDotConfigModel from 'models/PRTRLoDotConfig'
import { PUBLIC_URL } from '../config'

import fs from 'fs'
import Excel from 'exceljs'
import path from 'path'
import appRoot from 'app-root-path'
import md5 from 'md5'

import {
  get as _get,
  sum as _sum,
  map as _map,
  concat as _concat,
  forEach as _forEach,
  pick as _pick,
  round as _round,
  compact as _compact,
  find as _find,
  reduce as _reduce,
  set as _set,
  values as _values,
  uniq as _uniq,
  omit as _omit,
  filter as _filter,
  keyBy as _keyBy,
  findIndex as _findIndex,
  uniqBy as _uniqBy
} from 'lodash'

class PRTRReportDao {
  tinhToanEFofEnvi(congSuat, Env, heSo) {
    const value = (congSuat * Env) / heSo
    return value
  }

  tinhToanEFofEnvi_nmxlnt(congSuat, Env, heSo) {
    const value = (congSuat * Env * 365) / heSo
    return value
  }
  getDSChatDauVao_nmxlnt(congSuat, listConfigNMXLNuocThai) {
    const data = _map(listConfigNMXLNuocThai, (item, index) => {
      if (_get(item, 'value', null)) {
        const value =
          (congSuat * _get(item, 'value') * 365) / _get(item, 'fator')
        // console.log("--------", item.name )
        // console.log(value, "Ket qua" )
        // console.log( congSuat, _get(item, 'value'), _get(item, 'fator') )
        return {
          _id: index,
          name: item.name,
          until: 'Kg/năm',
          mass: _round(value, 6)
        }
      }
    })

    return _compact(data)
  }

  getEFValue_nmxlnt(congSuat, item, massDauVao) {
    // console.log(item,"resultItem")
    const khi = _get(item, 'Enviroment.khi', null)
    const heSo = _get(item, 'fator', null)
    // const massDauVao = _get(listNongDoDauVao[item.name],'mass', 0)
    if (khi && heSo) {
      const isCongSuat = _get(item, 'Recipe.congSuat', false)

      let value
      if (isCongSuat) {
        value = this.tinhToanEFofEnvi_nmxlnt(congSuat, khi, heSo)
        // console.log(item,isCongSuat, value, congSuat,"isCongSuat")
      } else {
        value = (massDauVao * khi) / heSo
        // console.log(item,isCongSuat,value,"isCongSuat")
      }

      return value
    } else {
      return null
    }
  }

  // NOTE res: data trong view, resPhantich: data trong modelPRTRFacility
  async customDataTheoCongThuc(res, ArrPPTT) {
    // console.log(res,"res")

    const dataTemplate = {
      khi: 0,
      nuocThai: 0,
      troBay: 0,
      xiLo: 0,
      taiCoSo_taiChe: 0,
      taiCoSo_xuLy: 0,
      bun: 0
    }
    let arr_CoSo_PhanTich = []
    const _promiseRun = _map(res, async (item, index) => {
      const dataPick = _pick(item, [
        '_id',
        'FacilityNameFull',
        'Latitude',
        'Longitude',
        'sector',
        'MM',
        'YYYY',
        'prtrlodots_docs',
        'prtrNuocThai_docs'
      ])
      // console.log("item",item)
      const PRTRKey = _get(item, 'sector.PRTRKey')
      //  console.log("item", PRTRKey)
      switch (PRTRKey) {
        case 'loDot':
          const item_loDot = _get(item, 'prtrlodots_docs')

          arr_CoSo_PhanTich.push({
            facility: _get(item, '_id'),
            MM: _get(item, 'MM'),
            YYYY: _get(item, 'YYYY')
          })
          // _get(item_loDot, 'config.measurings')
          const _PRTRLoDotConfigModel = await PRTRLoDotConfigModel.findOne({
            _id: _get(item_loDot, 'config')
          })
          const measurings = _get(_PRTRLoDotConfigModel, 'measurings')
          const congSuat = _get(item_loDot, 'capacityOperating')

          let HoaChats = []
          _forEach(ArrPPTT, item_ArrPPTT => {
            const strPP = item_ArrPPTT.value
            switch (strPP) {
              case 'EF': {
                const dataPPTT = _map(measurings, item_measurings => {
                  /* #region  cac gia tri tra ve */
                  let khiValue = 0
                  let taiCoSo_taiChe = 0
                  let taiCoSo_xuLy = 0
                  /* #endregion */

                  khiValue = this.tinhToanEFofEnvi(
                    congSuat,
                    _get(item_measurings, ['Enviroment', 'khi'], null),
                    _get(item_measurings, 'measuring.fator', null)
                  )
                  /* #region troBayValue   */

                  const troBayValue = this.tinhToanEFofEnvi(
                    congSuat,
                    _get(item_measurings, ['Enviroment', 'trobay'], null),
                    _get(item_measurings, 'measuring.fator', null)
                  )

                  const xiLoValue = this.tinhToanEFofEnvi(
                    congSuat,
                    _get(item_measurings, ['Enviroment', 'xilo'], null),
                    _get(item_measurings, 'measuring.fator', null)
                  )

                  /* #endregion */

                  /* #region   */

                  let taiChe_troBay = 0
                  if (
                    troBayValue &&
                    _get(item_loDot, 'intSite.taiChe.troBay')
                  ) {
                    taiChe_troBay =
                      troBayValue *
                      (_get(item_loDot, 'intSite.taiChe.troBay') / 100)
                  }

                  let taiChe_xilo = 0
                  if (xiLoValue && _get(item_loDot, 'intSite.taiChe.xiLo')) {
                    taiChe_xilo =
                      xiLoValue *
                      (_get(item_loDot, 'intSite.taiChe.xiLo') / 100)
                  }
                  taiCoSo_taiChe = taiChe_troBay + taiChe_xilo

                  /* #endregion */

                  /* #region  taiCoSo_xyLy */
                  /* #region   */

                  let xuLy_troBay = 0
                  if (troBayValue && _get(item_loDot, 'intSite.xuLy.troBay')) {
                    xuLy_troBay =
                      troBayValue *
                      (_get(item_loDot, 'intSite.xuLy.troBay') / 100)
                  }

                  let xuLy_xilo = 0
                  if (xiLoValue && _get(item_loDot, 'intSite.xuLy.xiLo')) {
                    xuLy_xilo =
                      xiLoValue * (_get(item_loDot, 'intSite.xuLy.xiLo') / 100)
                  }
                  taiCoSo_xuLy = xuLy_troBay + xuLy_xilo

                  /* #endregion */

                  return {
                    measuring: {
                      Name: _get(item_measurings, 'measuring.name')
                    },
                    PPTT: item_ArrPPTT,
                    ...dataTemplate,
                    khi: khiValue,
                    troBay: troBayValue,
                    xiLo: xiLoValue,
                    taiCoSo_taiChe: taiCoSo_taiChe,
                    taiCoSo_xuLy: taiCoSo_xuLy
                  }
                })

                HoaChats = _concat(HoaChats, dataPPTT)
                break
              }
              case 'DNNhap': {
                const dataKhonKhi = _map(
                  _get(item_loDot, 'khongKhi', []),
                  item_khongKhi => {
                    let khiValue = 0
                    khiValue = item_khongKhi.value
                    return {
                      measuring: _pick(item_khongKhi, ['Name']),
                      PPTT: item_ArrPPTT,
                      ...dataTemplate,
                      khi: khiValue
                    }
                  }
                )
                const dataTrobay = _map(
                  _get(item_loDot, 'troBay', []),
                  item_khongKhi => {
                    let khiValue = 0
                    khiValue = item_khongKhi.value
                    return {
                      measuring: _pick(item_khongKhi, ['Name']),
                      PPTT: item_ArrPPTT,
                      ...dataTemplate,
                      khi: khiValue
                    }
                  }
                )
                const dataXilo = _map(
                  _get(item_loDot, 'xiLo', []),
                  item_khongKhi => {
                    let khiValue = 0
                    khiValue = item_khongKhi.value
                    return {
                      measuring: _pick(item_khongKhi, ['Name']),
                      PPTT: item_ArrPPTT,
                      ...dataTemplate,
                      khi: khiValue
                    }
                  }
                )
                HoaChats = _concat(HoaChats, dataKhonKhi, dataTrobay, dataXilo)
                break
              }
              // case 'PhanTich': {
              //   break
              // }
              default:
                break
            }
          })
          // console.log("HoaChats")
          // console.log(HoaChats)
          item = {
            maDN: _get(item_loDot, 'maDN'),
            capacityOperating: _get(item_loDot, 'capacityOperating'),
            ...dataPick,
            HoaChats
          }

          return item

        case 'nmxlNuocThai':
          const item_nmxlnt = _get(item, 'prtrNuocThai_docs')
          const csHoatDong_nmxlnt = _get(item_nmxlnt, 'capacityOperating', null)
          const csThietKe_nmxlnt = _get(item_nmxlnt, 'capacityDesign', null)
          const congSuat_nmxlnt = csHoatDong_nmxlnt
            ? csHoatDong_nmxlnt
            : csThietKe_nmxlnt

          HoaChats = []
          _forEach(ArrPPTT, item_ArrPPTT => {
            const strPP = item_ArrPPTT.value
            switch (strPP) {
              case 'EF': {
                const listDSDauVao = this.getDSChatDauVao_nmxlnt(
                  congSuat_nmxlnt,
                  configNMXLNuocThai.data
                )

                const dataPPTT = _map(listDSDauVao, item_measurings => {
                  const resultItem = _find(
                    configNMXLNuocThai.data,
                    dataConfig_Item => {
                      return (
                        _get(dataConfig_Item, 'name', '') ===
                        _get(item_measurings, 'name', '')
                      )
                    }
                  )

                  const khiValue = this.getEFValue_nmxlnt(
                    congSuat_nmxlnt,
                    resultItem,
                    _get(item_measurings, 'mass')
                  )
                  // if(khiValue){
                  //   console.log(resultItem, khiValue,"resultItem")
                  // }

                  if (!khiValue) return null
                  return {
                    measuring: {
                      Name: _get(item_measurings, 'name')
                    },
                    PPTT: item_ArrPPTT,
                    ...dataTemplate,
                    khi: khiValue
                  }
                })

                const sanPhoiBun = _get(item_nmxlnt, 'sanPhoiBun')
                const mayEpBun = _get(item_nmxlnt, 'mayEpBun')
                const langTrongLuc = _get(item_nmxlnt, 'langTrongLuc')
                const mayLyTam = _get(item_nmxlnt, 'mayLyTam')

                const dataPPTT_2 = _map(
                  configNMXLNuocThai.data,
                  item_measurings => {
                    const giamAmBun = _get(
                      item_measurings,
                      'Enviroment.giamAmBun',
                      null
                    )

                    if (!giamAmBun) return null

                    // console.log(giamAmBun,"giamAmBun")

                    let bunValue = 0
                    if (sanPhoiBun || mayEpBun || langTrongLuc || mayLyTam) {
                      if (
                        sanPhoiBun > 0 &&
                        _get(giamAmBun, 'sanPhoiBun', null)
                      ) {
                        const heSo = _get(giamAmBun, 'sanPhoiBun', null)
                        bunValue +=
                          ((congSuat_nmxlnt * heSo * 365) / 1e6) *
                          (sanPhoiBun / 100)
                      }
                      if (mayEpBun > 0 && _get(giamAmBun, 'mayEpBun', null)) {
                        const heSo = _get(giamAmBun, 'mayEpBun', null)
                        bunValue +=
                          ((congSuat_nmxlnt * heSo * 365) / 1e6) *
                          (mayEpBun / 100)
                      }
                      if (
                        langTrongLuc > 0 &&
                        _get(giamAmBun, 'langTrongLuc', null)
                      ) {
                        const heSo = _get(giamAmBun, 'langTrongLuc', null)
                        bunValue +=
                          ((congSuat_nmxlnt * heSo * 365) / 1e6) *
                          (langTrongLuc / 100)
                      }
                      if (mayLyTam > 0 && _get(giamAmBun, 'mayLyTam', null)) {
                        const heSo = _get(giamAmBun, 'mayLyTam', null)
                        bunValue +=
                          ((congSuat_nmxlnt * heSo * 365) / 1e6) *
                          (mayLyTam / 100)
                      }
                    }
                    // console.log("----------")
                    // console.log(item_measurings)
                    // console.log(sanPhoiBun , mayEpBun , langTrongLuc , mayLyTam)
                    // console.log(bunValue)
                    if (!bunValue) return null

                    return {
                      measuring: {
                        Name: _get(item_measurings, 'name')
                      },
                      PPTT: item_ArrPPTT,
                      ...dataTemplate,
                      bun: bunValue
                    }
                  }
                )

                HoaChats = _concat(
                  HoaChats,
                  _compact(dataPPTT),
                  _compact(dataPPTT_2)
                )
                break
              }
              case 'PhanTich': {
                function getMValue(congSuat, item) {
                  const inputValue = item.value

                  if (inputValue) {
                    const value = (inputValue * congSuat * 365) / 1000
                    return value ? value : null
                  }
                  return null
                }
                const listNongDoSauXuLy = _get(
                  item_nmxlnt,
                  'nongDoSauXuLy',
                  null
                )

                // if(item._id=== "5d0c524d9242c64ae9f7b2ea"){
                //   console.log(listNongDoSauXuLy,"listNongDoSauXuLy")
                // }

                // console.log(listNongDoSauXuLy,"item_measurings")
                const dataPPTT = _map(listNongDoSauXuLy, item_measurings => {
                  if (!item_measurings.value || !item_measurings.Name)
                    return null

                  const nuocThai = getMValue(congSuat_nmxlnt, item_measurings)
                  // if(item._id=== "5d0c65019242c64ae9f7b30e"){
                  //   console.log("------")
                  //   console.log(nuocThai,"nuocThai",item_measurings.Name)
                  // }
                  if (nuocThai) {
                    return {
                      measuring: {
                        Name: _get(item_measurings, 'Name')
                      },
                      PPTT: item_ArrPPTT,
                      ...dataTemplate,
                      nuocThai: nuocThai
                    }
                  }
                })

                HoaChats = _concat(HoaChats, _compact(dataPPTT))
                break
              }
              default:
                break
            }
          })

          item = {
            maDN: _get(item_nmxlnt, 'maDN'),
            capacityOperating: _get(item_nmxlnt, 'capacityOperating'),
            ...dataPick,
            HoaChats
          }
          return item
        default:
          return item
      }
    })
    const result = await Promise.all(_promiseRun)

    let result_2
    if (arr_CoSo_PhanTich.length) {
      // console.log(arr_CoSo_PhanTich, '-------')
      // arr_CoSo_PhanTich = _uniqBy(arr_CoSo_PhanTich, 'facility')
      let distinct_arr = []
      _forEach(arr_CoSo_PhanTich , item =>{
        // console.log(distinct_arr.length,"---distinct_arr---")
        const isExits = _findIndex(distinct_arr, (itemlevel2)=>{
          return itemlevel2.facility.toString() == item.facility.toString()
        })
        // console.log(isExits,"----isExits----")
        if(isExits < 0){
          distinct_arr.push(item)
        }
      })

      // console.log(distinct_arr.length, arr_CoSo_PhanTich.length, '----_isExits---')

      result_2  = await Promise.all(
        _map(distinct_arr, async _item => {
          const _item_listPhantich = await modelPRTRFacility.findOne({
            facility: _get(_item, 'facility'),
            MM: _get(_item, 'MM'),
            YYYY: _get(_item, 'YYYY')
          })
          if (_item_listPhantich) {
            return this.tinhToanKetQuaQuanTracTheoCoSo(
              _item_listPhantich,
              ArrPPTT,
              dataTemplate
            )
          }
          return null
        })
      )
    }
    // return result_2
    // console.log("------")
    // console.log(result_2)
    return _compact( _concat(result, result_2))
  }

  tinhToanKetQuaQuanTracTheoCoSo(item_listPhantich, ArrPPTT, dataTemplate) {
    const ngayHoatDong = _get(item_listPhantich, 'ngayHoatDong')
    const listLoDot = _get(item_listPhantich, 'listLoDot', [])
    // console.log(item_listPhantich, "listLoDotlistLoDot")
    let HoaChats = []
    _forEach(listLoDot, item_lodot => {
      const luuLuong = _get(item_lodot, 'luuLuong')
      _forEach(ArrPPTT, item_ArrPPTT => {
        const strPP = item_ArrPPTT.value
        let khiValue = 0
        switch (strPP) {
          case 'PhanTich': {
            const dataPPTT = _map(item_lodot.hoachats, item_hoaChat => {
              const pow =
                item_hoaChat.hoaChat.Name === 'Dioxin/furan'
                  ? Math.pow(10, 12)
                  : Math.pow(10, 6)
              const ketpt = item_hoaChat.value
              let MValue = luuLuong * ketpt * 24 * ngayHoatDong

              khiValue = MValue / pow
              // console.log(khiValue, item_ArrPPTT, "khiValue")
              if (khiValue) {
                return {
                  measuring: {
                    Name: _get(item_hoaChat, 'hoaChat.Name')
                  },
                  PPTT: item_ArrPPTT,
                  ...dataTemplate,
                  khi: khiValue
                }
              }
            })
            HoaChats = _concat(HoaChats, dataPPTT)
            break
          }
          default:
            break
        }
      })
    })

    // return listLoDot
    const dataPick = _pick(item_listPhantich.facility, [
      '_id',
      'FacilityNameFull',
      'Latitude',
      'Longitude',
      'sector'
    ])
    return {
      //_pick(item_listPhantich,['_id','FacilityNameFull']),
      ...dataPick,
      HoaChats
    }
  }

  async reportGeneral(query = {}, PPTT) {
    // console.log(query,"query")
    const [res] = await Promise.all([
      viewPRTRcoSo.find(query, {
        _id: 1,
        FacilityNameFull: 1,
        Latitude: 1,
        Longitude: 1,
        sector: 1,
        prtrlodots_docs: 1,
        prtrNuocThai_docs: 1
      })
    ])

    if (!res) return
    // return res

    // const ArrPPTT = [
    //   { value: 'PhanTich', label: 'Đo đạc, phân tích' },
    //   { value: 'EF', label: 'Tính toán EF' },
    //   { value: 'DNNhap', label: 'Doanh nghiệp nhập' }
    // ]
    PPTT = _map(PPTT, stringify => {
      return JSON.parse(stringify)
    })
    // console.log(resPhantich,"resPhantich")

    const resultTong = await this.customDataTheoCongThuc(res, PPTT)
    // return resultTong
    // console.log(resultTong)
    const sumTheoChatvsPTTT = _reduce(
      _compact(resultTong),
      (prev, current) => {
        const hoaChats = _get(current, 'HoaChats', null)

        _forEach(hoaChats, item => {
          const Name = _get(item, 'measuring.Name', null)
          const PPTT = _get(item, 'PPTT.value', null)
          const _id = `${Name}_${PPTT}`
          // console.log(_id,"_id")
          if (!prev[_id]) {
            prev[_id] = item
            _set(prev[_id], ['listDN'], [current._id])
            return prev
          }

          const arrayKey = [
            'khi',
            'nuocThai',
            'troBay',
            'xiLo',
            'taiCoSo_taiChe',
            'taiCoSo_xuLy',
            'bun'
          ]

          _forEach(arrayKey, key => {
            let currentValue = 0
            let prevValue = 0
            /* #region  KHI THAI */
            currentValue = _get(item, [key], null)
            prevValue = _get(prev[_id], [key], null)

            if (currentValue) {
              _set(prev[_id], [key], prevValue + currentValue)
              let listCoSo = _get(prev[_id], ['listDN'])
              listCoSo = [...listCoSo, _get(current, '_id', null)]
              _set(prev[_id], ['listDN'], _uniq(listCoSo))
            }
          })
        })

        return prev
      },
      {}
    )

    // return {
    //   value:_compact(_values(sumTheoChatvsPTTT)) ,
    //   sumTheoChatvsPTTT
    // }

    const MergeData = _reduce(
      _compact(_values(sumTheoChatvsPTTT)),
      (prev, current) => {
        const Name = _get(current, 'measuring.Name', null)
        const _id = `${Name}`
        // console.log(_id,current, '_id')
        const PPTT = _omit(current, ['measuring'])
        if (!prev[_id]) {
          prev[_id] = {
            measuring: _get(current, 'measuring')
          }
          _set(prev[_id], ['PPTTs'], [PPTT])
          return prev
        }

        let listPPTTs = _get(prev[_id], ['PPTTs'])
        listPPTTs = [...listPPTTs, PPTT]
        _set(prev[_id], ['PPTTs'], listPPTTs)

        return prev
      },
      {}
    )

    // return MergeData
    return _values(MergeData)

    // return result
  }

  async reportGeneralBySulphite(query = {}, PPTT, hoaChat) {
    const [res] = await Promise.all([
      viewPRTRcoSo.find(query, {
        _id: 1,
        FacilityNameFull: 1,
        Latitude: 1,
        Longitude: 1,
        sector: 1,
        prtrlodots_docs: 1,
        prtrNuocThai_docs: 1
      })
    ])

    if (!res) return
    // return resPhantich

    PPTT = _map(PPTT, stringify => {
      return JSON.parse(stringify)
    })

    const resultTong = await this.customDataTheoCongThuc(res, PPTT)

    const NameHoaChat = _get(hoaChat, 'Name', '')
    // console.log(NameHoaChat)
    // return _compact(resultTong)

    // console.log(resultTong.length,"resultTong")
    const customData = _reduce(
      _compact(resultTong),
      (prev, current, index) => {
        const _id = _get(current, '_id', '').toString()

        let dataFillter = _filter(_get(current, 'HoaChats', []), item => {
          return _get(item, 'measuring.Name') === NameHoaChat
        })

        // dataFillter = _keyBy(dataFillter,'PPTT.value')
        // if(_id === '5bf616336bada61ba0cea30d' || _id === "5bf616336bada61ba0cea30d"){
        //   console.log(dataFillter,"5bf616336bada61ba0cea30d")
        //  }

        if (dataFillter.length === 0) return prev

        if (!prev[_id]) {
          prev[_id] = _omit(current, ['prtrlodots_docs', 'prtrNuocThai_docs'])
          _set(prev[_id], ['HoaChats'], {})
          // return prev
        }

        let currentHoaChats = _values(_get(prev[_id], ['HoaChats'], []))
        currentHoaChats = [...currentHoaChats, ...dataFillter]

        // if (_id === '5bf616336bada61ba0cea30d') {
        //   console.log(index)
        //   console.log('-----HoaChats------')
        //   console.log(_get(prev[_id], ['HoaChats']))
        //   // console.log('-----currentHoaChats------')
        //   // console.log(currentHoaChats)
        // }

        const dataHoaChats = _reduce(
          currentHoaChats,
          (prev_2, current_2) => {
            let _id_2 = _get(current_2, 'PPTT.value', null)
            // _id_2 = _id_2+ _get(current_2, 'measuring.Name', null)
            // console.log(_id_2,"_id_2")

            if (!_id_2) return prev_2

            // if(_id === '5bf616336bada61ba0cea30d'){
            //   console.log(current_2,"5bf616336bada61ba0cea30d")
            //   console.log(_id_2,"5bf616336bada61ba0cea30d")
            //  }

            if (!prev_2[_id_2]) {
              prev_2[_id_2] = current_2
              return prev_2
            }

            const arrayKey = [
              'khi',
              'nuocThai',
              'troBay',
              'xiLo',
              'taiCoSo_taiChe',
              'taiCoSo_xuLy',
              'bun'
            ]

            _forEach(arrayKey, key => {
              let currentValue = 0
              let prevValue = 0
              /* #region  KHI THAI */
              currentValue = _get(current_2, [key], null)
              prevValue = _get(prev_2[_id_2], [key], null)

              if (currentValue) {
                _set(prev_2[_id_2], [key], prevValue + currentValue)
              }
            })

            return prev_2
          },
          {}
        )
        // if(_id === '5bf616336bada61ba0cea30d' ){
        //   console.log("------AAAA-----")
        //   console.log(AAAA)
        //  }

        _set(prev[_id], ['HoaChats'], dataHoaChats)

        return prev
      },
      {}
    )
    // return customData
    return _values(customData)
  }

  processGenExcel(list, reportName, pathReportTemplate, columnsTemplate) {
    // console.log(columnsTemplate,"columnsTemplate")
    return new Promise((resolve, reject) => {
      if (!pathReportTemplate) {
        reject({
          error: true,
          message: 'Path report template not define'
        })
      }
      if (!columnsTemplate) {
        reject({
          error: true,
          message: 'Columns template not define'
        })
      }
      const reportName_frefix = reportName ? reportName : 'BC_Tong_Hop'
      let pathTemplate = path.join(
        __dirname,
        '../../templates',
        pathReportTemplate
      )
      const fileName = `data-${reportName_frefix}_${new Date().getTime()}.xlsx`
      let output = path.join(appRoot.path, 'output', fileName)
      if (!fs.existsSync(path.join(appRoot.path, 'output'))) {
        fs.mkdirSync(path.join(appRoot.path, 'output'))
      }

      let wb = new Excel.Workbook()
      wb.xlsx
        .readFile(pathTemplate)
        .then(async () => {
          let ws = wb.getWorksheet(1)

          //worksheet k0 cho phép lớn hơn 30 ký tự
          if (reportName_frefix.length && reportName_frefix.length > 30) {
            ws.name = reportName_frefix.substring(0, 27) + '...'
          } else ws.name = reportName_frefix

          let columns = columnsTemplate

          // Fixed value cho các cell ứng với file template.
          // ws.getCell('C2').value = _.get(organization, 'name', '')

          ws.columns = columns
          // ws.getRow(7).values = reportName_frefix
          // ws.getRow(8).values = row8
          let stt = 1

          const formatNumber = function formatNumber(value) {
            if (typeof value === 'number') {
              return value === 0 ? ' ' : value
            }
            return value
          }

          if (pathReportTemplate === 'report-template-tong-hop.xlsx') {
            _.forEach(list, item => {
              _forEach(_get(item, 'PPTTs', []), item_pttt => {
                const ctr =
                  _get(item_pttt, 'troBay', 0) + _get(item_pttt, 'xiLo', 0)
                const tong =
                  _get(item_pttt, 'khi', 0) +
                  _get(item_pttt, 'nuocThai', 0) +
                  ctr +
                  _get(item_pttt, 'bun', 0)
                const taiCoSo_tong =
                  _get(item_pttt, 'taiCoSo_taiChe', '') +
                  _get(item_pttt, 'taiCoSo_xuLy', '')
                const ngoaiCoSo_tong =
                  _get(item_pttt, 'ngoaiCoSo_taiChe', 0) +
                  _get(item_pttt, 'ngoaiCoSo_xuly', 0)
                ws.addRow({
                  stt: stt,
                  tenChat: _get(item, 'measuring.Name', ''),
                  pptt: _get(item_pttt, 'PPTT.label', ''),
                  sodn: formatNumber(
                    _uniq(_get(item_pttt, 'listDN', [])).length
                  ),
                  khi: formatNumber(_get(item_pttt, 'khi', '')),
                  nuoc: formatNumber(_get(item_pttt, 'nuocThai', '')),
                  ctr: formatNumber(ctr),
                  bun: formatNumber(_get(item_pttt, 'bun', '')),
                  tong: formatNumber(tong),
                  taiCoSo_taiChe: formatNumber(
                    _get(item_pttt, 'taiCoSo_taiChe', '')
                  ),
                  taiCoSo_xuly: formatNumber(
                    _get(item_pttt, 'taiCoSo_xuLy', '')
                  ),
                  taiCoSo_tong: formatNumber(taiCoSo_tong),
                  ngoaiCoSo_taiChe: formatNumber(
                    _get(item_pttt, 'ngoaiCoSo_taiChe', 0)
                  ),
                  ngoaiCoSo_xuly: formatNumber(
                    _get(item_pttt, 'ngoaiCoSo_xuly', 0)
                  ),
                  ngoaiCoSo_tong: formatNumber(ngoaiCoSo_tong)
                })
                let lastRow = ws.lastRow
                lastRow.font
                stt += 1
              })
            })
          } else if (
            pathReportTemplate === 'report-template-tong-hop-hoa-chat.xlsx'
          ) {
            _.forEach(list, (item, index) => {
              const EFValue = _sum([
                _get(item, 'HoaChats.EF.khi', 0),
                _get(item, 'HoaChats.EF.nuocThai', 0),
                _get(item, 'HoaChats.EF.troBay', 0),
                _get(item, 'HoaChats.EF.xiLo', 0),
                _get(item, 'HoaChats.EF.taiCoSo_taiChe', 0),
                _get(item, 'HoaChats.EF.taiCoSo_xuLy', 0),
                _get(item, 'HoaChats.EF.bun', 0)
              ])

              const DNNhapValue = _sum([
                _get(item, 'HoaChats.DNNhap.khi', 0),
                _get(item, 'HoaChats.DNNhap.nuocThai', 0),
                _get(item, 'HoaChats.DNNhap.troBay', 0),
                _get(item, 'HoaChats.DNNhap.xiLo', 0),
                _get(item, 'HoaChats.DNNhap.taiCoSo_taiChe', 0),
                _get(item, 'HoaChats.DNNhap.taiCoSo_xuLy', 0),
                _get(item, 'HoaChats.DNNhap.bun', 0)
              ])
              const phanTichValue = _sum([
                _get(item, 'HoaChats.PhanTich.khi', 0),
                _get(item, 'HoaChats.PhanTich.nuocThai', 0),
                _get(item, 'HoaChats.PhanTich.troBay', 0),
                _get(item, 'HoaChats.PhanTich.xiLo', 0),
                _get(item, 'HoaChats.PhanTich.taiCoSo_taiChe', 0),
                _get(item, 'HoaChats.PhanTich.taiCoSo_xuLy', 0),
                _get(item, 'HoaChats.PhanTich.bun', 0)
              ])

              ws.addRow({
                stt: stt,
                maDN: _get(item, 'maDN', ''),
                tenDN: _get(item, 'FacilityNameFull', ''),
                PhanTich: formatNumber(phanTichValue),
                EF: formatNumber(EFValue),
                DNNhap: formatNumber(DNNhapValue),
                tenNganh: formatNumber(_get(item, 'sector.Name', ''))
              })
              let lastRow = ws.lastRow
              // set cell to wrap-text
              ws.getCell(`B${lastRow._number}`).alignment = { wrapText: true }
              stt += 1
            })
          }

          const borderStyles = {
            top: { style: 'thin' },
            left: { style: 'thin' },
            bottom: { style: 'thin' },
            right: { style: 'thin' }
          }
          // Border cho row.
          ws.eachRow({ includeEmpty: false }, function(row, rowNumber) {
            // console.log(row._number,"row")
            if (row._number <= 9) return
            row.eachCell(function(cell, colNumber) {
              if (cell.value || cell.value === 0) {
                row.getCell(colNumber).border = borderStyles
              }
            })
          })

          // data: 'http://localhost:5004' + '/data-station-auto/download/' + fileName
          wb.xlsx
            .writeFile(output)
            .then(() =>
              resolve({
                data: PUBLIC_URL + 'PRTR-report/download/' + fileName
              })
            )
            .catch(err => reject(err))
        })
        .catch(err => reject(err))
    })
  }
}

export default new PRTRReportDao()
