import BaseDao from './baseDao'
import Log from 'models/Log'

export class LogDao extends BaseDao {
  constructor() {
    super(Log)
    this.sort={
      AddedOn: -1
    }
  }
}

export default new LogDao()